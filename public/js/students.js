// (function() {
    
// var roles_string = ['Viewer', 'Editor', 'Admin'];


// addUserCallback();
// function addUserCallback()
// {
//     let email = $("#user_email").text();

//     let query = db.collection('users').where('email', '==', email);

//     query.onSnapshot(function(snapshot) {
//         snapshot.docChanges().forEach(function(change) {


//             if (change.type === "added") {
//                 let role = change.doc.data().role;
//                 initial_role = role;
//                 setRole(role);
//                 if(role == 2 || role == 1)
//                 {
//                     $("#table_tr th").last().attr('id', 'actions_th');
//                 }
//             }

//             if (change.type === "modified") {
//                 setRole(change.doc.data().role);
//                 $("body").empty();
//                 $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

//                  setTimeout(() => {
//                     location.replace("index.html");
//                 }, 3000);
//             }

//             if (change.type === "removed") {
//             }
//         });
//     },
//         function(error){
//         console.log('retry add user callback');
//         addUserCallback();
//     });
// }

// function setRole(newRole)
// {
//     switch(newRole)
//     {
//         case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
//         case 1 : $(".role_admin").remove();break;
//     }
    
//     $("#user_role").text("Role: " + roles_string[newRole]);
//     $("#user_role").attr("data-done", true);
// }

// })();

'user strict'


var targetNode = document.getElementById('user_role');

var config = { attributes: true, childList: true, subtree: true };

var callback = function(mutationsList, observer) {
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

var observer = new MutationObserver(callback);




$(document).ready( function () {
    // observer.observe(targetNode, config);
    Start();
});

function Start()
{
    // initVariables();
    // attachButtonCallbacks();
    // initializeStudentsTable();
    // reloadStudentsDB();
    sessionStorage.setItem("toEdit", '');
    addButtonsCallback();
    initTable();
    addAccountsListener();
}

function addAccountsListener()
{
    db.collection("settings").doc("record").onSnapshot(function(doc) {
        // console.log("Current data: ", doc.data());
        var key = sessionStorage.getItem('toEdit');
        var isNew = (key == undefined || key.length == 0);
        if(isNew)
            $("#student_lrn").val(moment().format('YYYYMMDD') + '-' + padZero(doc.data().current_id_index, 3));
    });
}

var accounts_table;
var localAccountsData = [];

function initTable()
{
    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ]
    }



    accounts_table = $('#records_table').DataTable(
        {
        dom: 'Bflrtip',
        "scrollX": true,
        stateSave: true,
        columnDefs: [
        ],
        buttons: btns,        
        aoColumns: [
            { sClass: 'lrn center' },
            { sClass: 'l_name' },
            { sClass: 'f_name' },
            { sClass: 'm_name' },
            { sClass: 'gender' },
            { sClass: 'birthday' },
            { sClass: 'grade' },
            { sClass: 'section' },
            {
                bSortable: false,
                sClass: 'actions center'
            }
        ],
        initComplete: function(settings, json) {
            let buttons = $("#records_table_wrapper .dt-buttons button");
            buttons.addClass("btn");
            buttons.addClass("btn-outline-primary");
            buttons.removeClass("dt-button");

            addDBListener();
        }
    }
    );
}

function goSubmit(e)
{
    showLoading(true, "Saving...");
    var key = sessionStorage.getItem('toEdit');
    var isNew = (key == undefined || key.length == 0);
    // Check if ID or Email already exist.
    let id_query = db.collection('students').where('LRN', '==', $("#student_lrn").val());

    try
    {
        id_query.get().then(function(querySnapshot) {
            // console.log(querySnapshot.docs[0].data());

            if(querySnapshot.size > 0 && (isNew || querySnapshot.docs[0].id != key))
            {
                showLoading(false);
                bootbox.alert({
                    message:"Student LRN already in use!",
                    centerVertical: true,
                    className : "topmost-modal",
                    size : 'small'
                });
                
                sessionStorage.setItem("toEdit", '');

                return;
            }
            else
            // Form is valid, continue saving
            {                                 
                var ref = db.collection("students").doc();
				var isNew = false;

                if(key == undefined || key.length == 0)
                {
                    key = ref.id;
					isNew = true;
                }
                else
                {
                    ref = db.collection("students").doc(key);
                }
                
                sessionStorage.setItem("toEdit", "");

                let data = 
                {
                    LRN: $("#student_lrn").val(), 
                    first_name : $("#first_name").val().toLowerCase(),
                    name_ext_name : $("#name_ext_name").val().toLowerCase(),
                    middle_name : $("#middle_name").val().toLowerCase(),
                    last_name : $("#last_name").val().toLowerCase(), 
                    
                    gender: $("#gender").val(),
                    birthday: firebase.firestore.Timestamp.fromDate(moment($("#date_of_birth").val()).toDate()),
                    grade: $("#grade").val(), 
                    section: $("#section").val(),                     
                };

                setDataOnDBFromForm(data, ref);
				if(isNew)
				{
					console.log("isNew");
					createNewGradeRecords(key);
				}
            }

        });
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);        
    }
}


function createNewGradeRecords(key){
	var num_grade_card = 6;
	for(var i=0; i < num_grade_card; i++)
    {
	
		let newGradeDatas = {
			"subject_mathematics" :[0,0,0,0,0],
			"card_school_year" :"",
			"eligible_for_admission" :"",
			"card_school" :"",
			"learning_areas1" :[0,0,0,0,0],
			"makabansa1" :[0,0,0,0],
			"conducted_to" : new Date(),
			"card_division" :"",
			"card_school_id" :"",
			"card_adviser" :"",
			"card_classified" :"",
			"makatao" :[0,0,0,0],
			"conducted_from" : new Date(),
			"subject_pe" :[0,0,0,0,0],
			"subject_mother_tongue" :[0,0,0,0,0],
			"subject_health" :[0,0,0,0,0],
			"subject_araling_panlipunan" :[0,0,0,0,0],
			"subject_epp_tle" :[0,0,0,0,0],
			"subject_arts" :[0,0,0,0,0],
			"subject_science" :[0,0,0,0,0],
			"subject_islamic" :[0,0,0,0,0],
			"learning_areas2" :[0,0,0,0,0],
			"card_district" :"",
			"subject_filipino" :[0,0,0,0,0],
			"subject_arabic" :[0,0,0,0,0],
			"card_region" :"",
			"subject_english" :[0,0,0,0,0],
			"card_section" :"",
			"subject_music" :[0,0,0,0,0],
			"makabansa2" :[0,0,0,0],
			"card_adviser" :"",
			"card_region" :"",
			"subject_music" :[0,0,0,0,0],
			"subject_pe" :[0,0,0,0,0],
			"card_school" :"",
			"subject_english" :[0,0,0,0,0],
			"subject_arabic" :[0,0,0,0,0],
			"card_district" :"",
			"subject_filipino" :[0,0,0,0,0],
			"subject_araling_panlipunan" :[0,0,0,0,0],
			"subject_islamic" :[0,0,0,0,0],
			"eligible_for_admission" :"",
			"subject_arts" :[0,0,0,0,0],
			"subject_epp_tle" :[0,0,0,0,0],
			"learning_areas1" :[0,0,0,0,0],
			"makabansa1" :[0,0,0,0],
			"subject_health" :[0,0,0,0,0],
			"card_section" :"",
			"subject_science" :[0,0,0,0,0],
			"card_school_year" :"",
			"subject_mother_tongue" :[0,0,0,0,0],
			"card_school_id" :"",
			"learning_areas2" :[0,0,0,0,0],
			"makatao" :[0,0,0,0],
			"card_classified" :"",
			"makabansa2" :[0,0,0,0],
			"subject_mathematics" :[0,0,0,0,0],
			"card_division" :"",
			"subject_pe" :[0,0,0,0,0],
			"subject_english" :[0,0,0,0,0],
			"subject_science" :[0,0,0,0,0],
			"card_adviser" :"",
			"card_section" :"",
			"card_classified" :"",
			"card_school_id" :"",
			"subject_arabic" :[0,0,0,0,0],
			"subject_music" :[0,0,0,0,0],
			"card_region" :"",
			"makabansa1" :[0,0,0,0],
			"subject_araling_panlipunan" :[0,0,0,0,0],
			"eligible_for_admission" :"",
			"subject_arts" :[0,0,0,0,0],
			"subject_health" :[0,0,0,0,0],
			"learning_areas2" :[0,0,0,0,0],
			"subject_epp_tle" :[0,0,0,0,0],
			"subject_mathematics" :[0,0,0,0,0],
			"subject_mother_tongue" :[0,0,0,0,0],
			"card_school_year" :"",
			"makabansa2" :[0,0,0,0],
			"subject_filipino" :[0,0,0,0,0],
			"card_school" :"",
			"makatao" :[0,0,0,0],
			"learning_areas1" :[0,0,0,0,0],
			"card_district" :"",
			"card_division" :"",
			"subject_islamic" :[0,0,0,0,0],
			"eligible_for_admission" :"",
			"card_adviser" :"",
			"card_division" :"",
			"subject_science" :[0,0,0,0,0],
			"card_school_year" :"",
			"card_district" :"",
			"subject_pe" :[0,0,0,0,0],
			"card_classified" :"",
			"subject_health" :[0,0,0,0,0],
			"card_school" :"",
			"subject_music" :[0,0,0,0,0],
			"subject_english" :[0,0,0,0,0],
			"subject_epp_tle" :[0,0,0,0,0],
			"card_region" :"",
			"learning_areas2" :[0,0,0,0,0],
			"subject_araling_panlipunan" :[0,0,0,0,0],
			"subject_arabic" :[0,0,0,0,0],
			"card_school_id" :"",
			"card_section" :"",
			"undefined" :[0,0,0,0,0],
			"subject_arts" :[0,0,0,0,0],
			"makatao" :[0,0,0,0],
			"subject_filipino" :[0,0,0,0,0],
			"subject_islamic" :[0,0,0,0,0],
			"makabansa2" :[0,0,0,0],
			"subject_mother_tongue" :[0,0,0,0,0],
			"subject_mathematics" :[0,0,0,0,0],
			"learning_areas1" :[0,0,0,0,0],
			"makabansa1" :[0,0,0,0],
			"makatao" :[0,0,0,0],
			"makabansa2" :[0,0,0,0],
			"card_school" :"",
			"card_school_year" :"",
			"makabansa1" :[0,0,0,0],
			"eligible_for_admission" :"",
			"makabansa2" :[0,0,0,0],
			"card_school" :"",
			"makatao" :[0,0,0,0],
			"makabansa1" :[0,0,0,0],
			"eligible_for_admission" :"",
			"card_school_year" :""
		}
	
		newGradeDatas['maka-kalikasan'] = [0,0,0,0];
		newGradeDatas['maka-diyos'] = [0,0,0,0];
		
		for (var k in newGradeDatas) {
			if(newGradeDatas[k] == undefined)
				console.log(k + "=" + newGradeDatas[k]);
		}
	
		db.collection("grade"+i).doc(key).set(newGradeDatas);
	}
}

function addDBListener()
{
    showLoading(true, "Loading students...");
    try {

        db.collection("students")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                var key = change.doc.id;
                
                if (change.type === "removed") {

                    delete localAccountsData[change.doc.data().LRN];

                    accounts_table.row('#'+key).remove().draw();                   
                }
                else
                {
                    let data = change.doc.data();
                    localAccountsData[data.LRN] = data;
                    localAccountsData[data.LRN]["key"] = change.doc.id;
                    
                    var d = {grade:"", section:"", last_name:"", name_ext_name:""};
                    if('LRN' in change.doc.data())
                        d['LRN'] = change.doc.data().LRN;
                    
                    if('first_name' in change.doc.data())
                        d['first_name'] = toTitleCase(change.doc.data().first_name);
                        
                    if('name_ext_name' in change.doc.data())
                        d['name_ext_name'] = toTitleCase(change.doc.data().name_ext_name);

                    if('middle_name' in change.doc.data())
                        d['middle_name'] = toTitleCase(change.doc.data().middle_name);

                    if('last_name' in change.doc.data())
                        d['last_name'] = toTitleCase(change.doc.data().last_name);

                    if('gender' in change.doc.data())
                        d['gender'] = change.doc.data().gender;

                    if('birthday' in change.doc.data())
                        d['birthday'] = change.doc.data().birthday;

                    if('grade' in change.doc.data())
                        d['grade'] = change.doc.data().grade;

                    if('section' in change.doc.data())
                        d['section'] = change.doc.data().section;

                    let newData =
                    [
                        d.LRN,
                        d.last_name,
                        d.first_name + " " + d.name_ext_name,
                        d.middle_name,             
                        d.gender,
                        moment(d.birthday.toDate()).format("MM-DD-YYYY"),
                        d.grade,
                        d.section           
                    ];

                    if (change.type === "added") {
                        // if(document.getElementById("actions_th") != null)
                        newData.push(getNewActionRow(change.doc.id));
                        // else
                        //     newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');              
                        let newRow = accounts_table.row.add(newData).node().id = change.doc.id;
    
                        accounts_table.draw( false );
                    }
                    
                    if (change.type === "modified") {
    
                        // let data = change.doc.data();
                        // localStudentsData[data.LRN] = data;
                        // localStudentsData[data.LRN]["key"] = change.doc.id; 
                        let tr = $('.remove-row[data-key="'+key+'"]').parent().parent();                    
    
                        let updatedData = [
                            d.LRN,
                            toTitleCase(d.last_name),
                            toTitleCase(d.first_name + " " + d.name_ext_name),
                            toTitleCase(d.middle_name),
                            d.gender,
                            moment(d.birthday.toDate()).format("MM-DD-YYYY"),
                            d.grade,
                            d.section,                        
                            getNewActionRow(key)
                        ];
    
                        accounts_table.row(tr).data(updatedData).draw();                 
                    }                    
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {showLoading(false);});
        },

        function(error){

            bootbox.alert({
                centerVertical:true,
                size:'small',
                message:error.message
            }
                );
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}

function getNewActionRow(key)
{
    return  '<a data-key="'+key+'" href="#" onclick="gotoRecord(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>'+
            '<a data-key="'+key+'" href="#" onclick="showPromptEdit(this);" class="on-default edit-row mx-2" data-toggle="tooltip" data-placement="top" title="edit student information"><i class="fa fa-pencil"></i></a>'+
            '<a data-key="'+key+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2" data-toggle="tooltip" data-placement="top" title="delete student" style="color:red;"><i class="fa fa-trash-o"></i></a>';
}

function gotoRecord(elem)
{
    var key = $(elem).attr('data-key');
    sessionStorage.setItem("selectedRecord", key);
    window.open("records.html", '_blank');
}


function setDataOnDBFromForm(data, ref)
{
    ref.set(data, {merge:true})
    .then(function(docRef) {
        const increment = firebase.firestore.FieldValue.increment(1);

        // Document reference
        const current_id_index = db.collection('settings').doc('record');

        // Update read count
        current_id_index.update({ current_id_index: increment });

        showNotifSuccess('Saved', capitalizeFLetter(data.LRN) + ' saved successfully');
		
		addStudentLog('Student #' + data['LRN'] + " updated by " + $("#user_email").text(), "default")
        showLoading(false);

        // sessionStorage.setItem("toEdit", '');

        $('#editStudentModal').modal('hide');
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
        PNotify.error({
          title: 'Error',
          text: error
      });
        showLoading(false);
    });
}


function showPromptDelete(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();
    let student_lrn = accounts_table.row(tr).data()[0];
    bootbox.confirm({ 
        size: "large",
        centerVertical : true,
        title: "Are you sure?",
        message : "Deleting a student will also delete his/her record, make sure you have a backup!</b>",
        callback: function(result){ 
            if(result)
            {
                showLoading(true, "Deleting...");
                try
                {

                    db.collection("students").doc(key).delete().then(function() {
						var num_grade_card = 6;
						for(var i=0; i < num_grade_card; i++){
							db.collection("grade"+i).doc(key).delete();
						}
						
                        showNotifSuccess(student_lrn + " record successfully deleted!");
						addStudentLog('Student #' + student_lrn + " deleted by " + $("#user_email").text(), "removal");
                    }).catch(function(error) {
                        showNotifError("Error removing document: " + error.message);
                    });

                    
                }
                catch(error)
                {
                    showLoading(false);
                    bootbox.alert(error.message);
                }
            }

            sessionStorage.setItem("toEdit", '');
        }
    })
}

function showPromptEdit(elem)
{
	$("#editStudentModalLabel").text("Manage Student");
    var key = $(elem).attr('data-key');
    sessionStorage.setItem("toEdit", key);


    var tr = $(elem).parent().parent();

    var myRow = accounts_table.row(tr);

    let LRN = myRow.data()[0];
    // Fill form based on selected student data.    
    $("#name_ext_name").val(localAccountsData[LRN].name_ext_name);
    $("#student_lrn").val(LRN);
    $("#last_name").val(myRow.data()[1]);
    $("#first_name").val(localAccountsData[LRN].first_name);
    $("#middle_name").val(myRow.data()[3]);
    $("#gender").val(myRow.data()[4]);
    $("#date_of_birth").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).datepicker("update", myRow.data()[5]); 
    //$("#grade").val(myRow.data()[6]);
	let selectedGrade = (myRow.data()[6] == null || myRow.data()[6] == undefined || myRow.data()[6].length == 0)? 1 : myRow.data()[6];	
	$('#grade option[value="'+selectedGrade+'"]').prop('selected','selected');
    $("#section").val(myRow.data()[7]);    

    $("#editStudentModal").modal('show');
}

function fetchCurrentIdIndex()
{
    //Get current id index
    db.collection("settings").doc("record").get().then(function(doc) {
        $("#student_lrn").val(moment().format('YYYYMMDD') + '-' + padZero(doc.data().current_id_index, 3));
    }).catch(function(error) {
        console.log("Error getting cached document:", error);
    });
}



function addButtonsCallback()
{
    $('#editStudentModal').modal(
    {
        keyboard: false,
        backdrop: 'static',
        show:false
    });

    $('#date_of_birth').datepicker({
        autoclose:true,
        endDate:'-7y',
        disableTouchKeyboard: true
    });


    $("#addToTable").on('click', function(){
        sessionStorage.setItem('toEdit', '');
        $("#editStudentForm").get(0).reset();
        fetchCurrentIdIndex();
		$("#editStudentModalLabel").text("Add Student");
    });

    $("#importToTable").on('click', function(){
        showImportModal();
    });

    $("#editStudentForm").keypress(function(event){

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          if($("#editStudentForm").get(0).reportValidity())
            goSubmit(event);
          //alert('You pressed a "enter" key in somewhere');
        }
      });

      $("#submitButton").on('click', function(e){
        if($("#editStudentForm").get(0).reportValidity())
          goSubmit(e);
      });

    $("#editStudentModal").on('hidden.bs.modal', function(m){
        sessionStorage.setItem('toEdit', '');
    });

	/* $("#editStudentModal").on('shown.bs.modal', function(){
        
		console.log("editModalShown : " + selectedGrade);
		
    }); */


    addPastePrevent($("form input"));
	
	$(".upperCaseInput").on('input',function(e){
		$(this).val(jsUcfirst($(this).val()));
	});

}


function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}