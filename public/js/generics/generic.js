$(document).ready(function () {

    // var is_root = location.pathname == "/";
    var is_root = window.location.href.includes('index.html') || window.location.toString().substr(window.location.toString().length - 1) == "/";
    if(is_root == false){    
		$.unblockUI();
    } 


    PNotify.defaultModules.set(PNotifyBootstrap4, {});
    PNotify.defaultModules.set(PNotifyFontAwesome5Fix, {});
    PNotify.defaultModules.set(PNotifyFontAwesome5, {});
    PNotify.defaultModules.set(PNotifyAnimate, {});
    PNotify.defaultModules.set(PNotifyMobile, {swipeDismiss: false, styling: true});


    $('body').on('keyup', '.inputNumericOnly', function(e){
        
        let str = $(this).val().replace(/[^0-9]/g, '');
        $(this).val(str);
    });
    



    // $('body').on('keyup', '.inputNumericOnlyDiv', function(e){
        
    //     if(e.key != "ArrowRight" && e.key != "ArrowLeft" && e.key != "ArrowUp" && e.key != "ArrowDown")
    //     {
    //         let str = $(this).text().replace(/[^0-9]/g, '');

    //         let pos = getCaretPosition(this);
    //         console.log(pos);
    //         $(this).text(str); 
            
            
    //         setCaret(this, pos);           
    //     }
    // });


    $('body').on('keyup', '.inputNumericOnlyWithDash', function(e){
        let str = $(this).val().replace(/[^0-9\-]/g, '');
        console.log("keyup!");

        $(this).val(str);
    });

  
	firebase.auth().onAuthStateChanged(function(user) {
	  if (user) {
		  
	  }
	  else
	  {
		  if(is_root == false && !window.location.href.includes('reset.html'))
			location.replace("index.html");
	  }
	});
	
	$('#logout').on('click', function()
	{
		logOut();
	});
	
	
	$(".alpha-only").on("keydown", function(event){
	  // Allow controls such as backspace, tab etc.
	  var arr = [8,9,16,17,20,35,36,37,38,39,40,45,46,32];

	  // Allow letters
	  for(var i = 65; i <= 90; i++){
		arr.push(i);
	  }

	  // Prevent default if not in array
	  if(jQuery.inArray(event.which, arr) === -1){
		event.preventDefault();
	  }
	});
	
	/* $(".alpha-only").on("input", function(){
	  var regexp = /[^a-zA-Z]/g;
	  if($(this).val().match(regexp)){
		$(this).val( $(this).val().replace(regexp,'') );
	  }
	}); */
	
});

//log out functionality
function logOut()
{
	
	firebase.auth().signOut().then(() => {
	  // Sign-out successful.
	  addAccountLog('Account Log out ' + $("#user_email").text() , "default");
	}).catch((error) => {
	  // An error happened.
	});
}

Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}

Date.prototype.addMinutes= function(m){
    this.setMinutes(this.getMinutes()+m);
    return this;
}


function addPastePrevent(elem)
{
    
    elem.each(function(e, i){

        var dom = elem.get(e);
        dom.onpaste = function(e) {
        e.preventDefault();
    }
    });
    
}

function padZero(str, len) {  
    str = str + ""  ;
    len = len || 2;

    if(str.length > len)
        return str;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
}

function capitalizeFLetter(str) { 
    if(str == undefined)
        return "";
    let res = str[0].toUpperCase() + str.slice(1); 
    return res;
} 


function showNotifSuccess(_title, _text)
{
    PNotify.notice({
      title: _title,
      text: _text,
      delay : 2000,
      styling: 'custom_success',
      // maxTextHeight: null,
      // addModelessClass: 'nonblock',
      icon: 'fas fa-check-circle'
      // ,      
      // buttons: { closer: true, 
      //     labels: {close: "Fechar", stick: "Manter"}
      // }
    });
}

function showNotifError(_title, _text)
{
    PNotify.notice({
      title: _title,
      text: _text,
      delay : 2000,
      styling: 'custom_error',
      // maxTextHeight: null,
      // addModelessClass: 'nonblock',
      icon: 'fas fa-exclamation-circle'
    });
}

const toTitleCase = (phrase) => {
    return phrase
      .toLowerCase()
      .split(' ')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  };

function removeLeadingZero(elem)
{
	var val = elem.val();

	elem.val(val.replace(/^0+/, ''));
}

function fitText(outputDiv, maxFontSize){
    // max font size in pixels
    // const maxFontSize = 50;
    // get the DOM output element by its selector
    //let outputDiv = document.getElementById(outputSelector);
    // get element's width
    let extra_padding = 5;
    let width = outputDiv.clientWidth;
    // get content's width
    let contentWidth = outputDiv.scrollWidth;
    // get fontSize
    let fontSize = parseInt(window.getComputedStyle(outputDiv, null).getPropertyValue('font-size'),10);
    // if content's width is bigger then elements width - overflow
    if (contentWidth > width){
        fontSize = Math.ceil(fontSize * width/contentWidth,10);
        fontSize =  fontSize > maxFontSize  ? fontSize = maxFontSize  : fontSize - 1;
        outputDiv.style.fontSize = fontSize-extra_padding+'px';   
    }else{
        // content is smaller then width... let's resize in 1 px until it fits 
        while (contentWidth === width && fontSize < maxFontSize){
            fontSize = Math.ceil(fontSize) + 1;
            fontSize = fontSize > maxFontSize  ? fontSize = maxFontSize  : fontSize;
            outputDiv.style.fontSize = fontSize-extra_padding+'px';   
            // update widths
            width = outputDiv.clientWidth;
            contentWidth = outputDiv.scrollWidth;
            if (contentWidth > width){
                outputDiv.style.fontSize = fontSize-extra_padding+'px'; 
            }
        }
    }
}

function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function preventNumberInput(e){
    var keyCode = (e.keyCode ? e.keyCode : e.which);
    if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107 ){
        e.preventDefault();
    }
}
//prevent contenteditable caret reset

// function setCaret(el, prevPos) {
//     // var el = document.getElementById("editable")
//     var range = document.createRange();
//     var sel = window.getSelection();
    
//     range.setStart(el, prevPos);
//     range.collapse(true);
    
//     sel.removeAllRanges();
//     sel.addRange(range);
// }

// function getCaretPosition(element) {
//         var caretOffset = 0;
//         var doc = element.ownerDocument || element.document;
//         var win = doc.defaultView || doc.parentWindow;
//         var sel;
//         if (typeof win.getSelection != "undefined") {
//             sel = win.getSelection();
//             if (sel.rangeCount > 0) {
//                 var range = win.getSelection().getRangeAt(0);
//                 var preCaretRange = range.cloneRange();
//                 preCaretRange.selectNodeContents(element);
//                 preCaretRange.setEnd(range.endContainer, range.endOffset);
//                 caretOffset = preCaretRange.toString().length;
//             }
//         } else if ( (sel = doc.selection) && sel.type != "Control") {
//             var textRange = sel.createRange();
//             var preCaretTextRange = doc.body.createTextRange();
//             preCaretTextRange.moveToElementText(element);
//             preCaretTextRange.setEndPoint("EndToEnd", textRange);
//             caretOffset = preCaretTextRange.text.length;
//         }
//         return caretOffset;
//     }