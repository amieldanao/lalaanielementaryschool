(function() {
    // just drag your whole code from your script.js and drop it here
// var myRole = 0;

addPageAccessCallback();
// var initial_role;
// var myRole;



function addPageAccessCallback()
{

    // showLoading(true, "Loading admin user");
    var is_root = window.location.href.includes('reset.html') || window.location.href.includes('index.html') || window.location.toString().substr(window.location.toString().length - 1) == "/";
    if(is_root == false){
    // var is_root = location.pathname == "/";
    // if(!is_root){
    
    // $("#dimScreen").css({'height':'100%', 'margin' : 'auto', 'vertical-align':'middle'});
        if($('#dimScreen').length > 0){
            $("#dimScreen").prepend('<img src="images/school_logo.png"/>');
            $("#dimScreen").append('<img src="images/loader.gif" />');
            $.blockUI({fadeIn: 1, message: $('#dimScreen') });
        }
        
    }

    firebase.auth().onAuthStateChanged(function(user) {
        if(is_root == false)
        {
            if (user) {
                

                // check if this role has page access
                let role_query = db.collection('accounts').where('email', '==', user.email.toLowerCase());
                $("#user_email").text(user.email.toLowerCase());

                
                role_query.get().then(function(roleQuerySnapshot){
                    if(roleQuerySnapshot.size > 0)
                    {
                        // Set username
                        let myName = roleQuerySnapshot.docs[0].data().name;
                        $("#user_email").attr('data-myName', myName);

                        setResult('');
                    }
                    else
                    {
                        setResult('un-authorized');
                    }

                })
                .catch(function(error){
                    setResult('un-authorized');
                });

            } else {
                if(is_root == false)
                {
                    setResult('un-authorized');
                }
            }
        }
        else
        {
            setResult('');
        }

    });
}


function setResult(result)
{
    $("footer").attr("data-done", result);
}

})();


var targetNode = document.getElementsByTagName('footer')[0];

// Options for the observer (which mutations to observe)
// const config = { attributes: true, childList: true, subtree: true };
var config = { attributes: true};

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    // Use traditional 'for loops' for IE 11
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            // console.log('The ' + mutation.attributeName + ' attribute was modified.');

            // console.log('A child node has been added or removed.');
            Start();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);


observer.observe(targetNode, config);
var newMessageListeners = [];


function Start()
{

    let result = $('footer').attr('data-done');
    if(result == "no-acess")
        showNoAccessPage();
    else if(result == "un-authorized")
        showUnAuthorized();
    else
        LoadScripts();

}


$(document).ready(function(){
    PNotifyDesktop.permission();
});

function showNoAccessPage()
{
    document.head.innerHTML = "";
    document.body.innerHTML = '<h1>You cannot access this page, please contact the admin to grant you permission.</h1>';
}

function showUnAuthorized()
{
    document.head.innerHTML = "";
    document.body.innerHTML = '<h1>Unauthorized access! Please log in <a href="index.html">here</a> first</h1>';
}

function LoadScripts(async)
{
    if( async === undefined ) {
        async = false;
    }
    var scripts = [];
    var _scripts = ['js/generics/generic.js', 'js/generics/loading.js'];
    
    // console.log(window.location.href);
    let splitted_link = window.location.href.split("/");
    let current_page = splitted_link[splitted_link.length-1];

    switch(current_page.replace(/#/g, ''))
    {
        case "home.html" :   break;
        case "accounts.html" :  _scripts.push('js/accounts.js'); break;
        case "records.html" :  _scripts.push('js/records.js'); break;
        case "students.html" :  _scripts.push('js/students.js'); break;
    }
	
	_scripts.push('js/logger.js');
    if(window.location.href.includes('reset.html'))
    {
        _scripts.push('js/reset.js');
    }

    if(async){
        LoadScriptsAsync(_scripts, scripts)
    }else{
        LoadScriptsSync(_scripts,scripts)
    }
}

// what you are looking for :
function LoadScriptsSync (_scripts, scripts) {

    var x = 0;
    var loopArray = function(_scripts, scripts) {
        // call itself
        loadScript(_scripts[x], scripts[x], function(){
            // set x to next item
            x++;
            // any more items in array?
            if(x < _scripts.length) {
                loopArray(_scripts, scripts);   
            }
        }); 
    }
    loopArray(_scripts, scripts);      
}

// async load as in your code
function LoadScriptsAsync (_scripts, scripts){
    for(var i = 0;i < _scripts.length;i++) {
        loadScript(_scripts[i], scripts[i], function(){});
    }
}

// load script function with callback to handle synchronicity 
function loadScript( src, script, callback ){

    script = document.createElement('script');
    script.onerror = function() { 
        // handling error when loading script
        alert('Error to handle')
    }
    script.onload = function(){
        // console.log(src + ' loaded ')
        callback();
    }
    script.src = src;
    document.getElementsByTagName('head')[0].appendChild(script);
}



