

var key_editing;
var num_grade_card = 6;
var core_values = ["Maka-Diyos", "Makatao", "Maka-kalikasan", "Makabansa1", "Makabansa2"];
var fixed_subjects = ["subject_general_average"];
var subjects_to_remove = new Array();
var subjects_to_save = new Array();


$(document).ready( function () {
    // observer.observe(targetNode, config);
    Start();
});



function Start()
{
    key_editing = sessionStorage.getItem("selectedRecord");
    generateCardsLayout();
    
}

function generateCardsLayout()
{
    $("#card_container").find(".grade_card").each(function( index ) {
    
        $(this).append(card_template);
    });
    $("#card_container2").find(".grade_card").each(function( index ) {
    
        $(this).append(card_template2);
    });

    $("#card_container2").append(form137_bottom);
    $("#card_container2").append(part_4);

    fetchStudentRecord(key_editing);
    addButtonsCallback();
    // card_template
}

function save()
{
    var isNew = (key_editing == undefined || key_editing.length == 0);

    if(isNew){
        bootbox.alert("Error in document, please re-select a record in students table!");
        return;
    }

    if($("form").get(0).reportValidity() == false || $("form").get(1).reportValidity() == false)
    {
        bootbox.alert("Please fill up all required fields!");
        return;
    }
   
    showLoading(true, "Saving...");


    //dates
    let var_date_assessment = null;
    if($("#date_assessment").val().trim().length > 0)
    {
        var_date_assessment = firebase.firestore.Timestamp.fromDate(moment($("#date_assessment").val(), 'MM/DD/YYYY').toDate());
    }

    let var_date_designation = null;
    let temp_date_designation = moment($("#date_designation").val(), 'MM/DD/YYYY').toDate();
    if(temp_date_designation instanceof Date && !isNaN(temp_date_designation))
      date_designation = firebase.firestore.Timestamp.fromDate(temp_date_designation);
    // Save Personal Information
    let personal_data = 
    {
        LRN : $("#student_lrn").val(),
        birthday: firebase.firestore.Timestamp.fromDate(moment($("#date_of_birth").val(), 'MM/DD/YYYY').toDate()),
        first_name: $("#first_name").val(),
        last_name: $("#last_name").val(),
        middle_name: $("#middle_name").val(),
        name_ext_name: $("#name_ext_name").val(),
        gender: $("#gender").val(),

        kinder_progress_report: $("#kinder_progress_report").get(0).checked,
        eccd_checklist: $("#eccd_checklist").get(0).checked,
        kinder_certificate_completion: $("#kinder_certificate_completion").get(0).checked,
        
        name_of_school : $("#name_of_school").val(),
        school_id : $("#school_id").val(),
        address_of_school : $("#address_of_school").val(),

        pept_passer_rating : $("#pept_passer_rating").val(),
        date_assessment : var_date_assessment,
        others_eligibility : $("#others_eligibility").val(),
        testing_center : $("#testing_center").val(),
        remark_eligibility : $("#remark_eligibility").val(),

        //certificate of transfer
        record_of : $("#record_of").text(),
        he_she_eligible : $("#he_she_eligible").text(),
        date_designation : var_date_designation,

        guardian : $("#guardian").val(),
        guardian_address : $("#guardian_address").val(),
        guardian_occupation : $("#guardian_occupation").val(),
    };


    let batch = db.batch();

    let cRef = db.collection('students').doc(key_editing);
    batch.set(cRef, personal_data);

    let sRef = new Array(num_grade_card);
    let aRef = new Array(num_grade_card);
    let removeRef = new Array(num_grade_card);
    let attendance_data = {};

    for(var i=0; i < num_grade_card; i++)
    {
         let grade_data = {};

         if($('.grade_form').length > i)
         {

         
         let parent = $(".grade_form")[i];
         let var_conducted_from = null;
         let var_conducted_to = null;

         let conducted_from = $(parent).find(".conducted_from");

         if(conducted_from.val().length > 0)
               var_conducted_from = firebase.firestore.Timestamp.fromDate(moment($(parent).find("#conducted_from", "MM/DD/YYYY").val()).toDate());

         if($(parent).find(".conducted_to").val().length > 0)
               var_conducted_to = firebase.firestore.Timestamp.fromDate(moment($(parent).find("#conducted_to").val()).toDate());

         grade_data =
         {
               card_school_id : $(parent).find("#card_school_id").val(),
               card_district : $(parent).find("#card_district").val(),
               card_division : $(parent).find("#card_division").val(),
               card_region : $(parent).find("#card_region").val(),
               card_classified : $(parent).find("#card_classified").val(),
               card_section : $(parent).find("#card_section").val(),               
               card_adviser : $(parent).find("#card_adviser").val(),
               // card_signature : $(parent).find("#card_signature").val(),
         
               conducted_from : var_conducted_from,
               conducted_to : var_conducted_to,

               // school_year : $($("#card" + (i+1) + "B").find("#card_school_year").get(0)).text()
         };

         $(parent).find(".subject").each(function( index ) {
               let subjectTitle = $(this).attr('data-key');
			   console.log("Saving... ");
			   console.log($(this));
               grade_data[subjectTitle] = new Array(5);
               grade_data[subjectTitle][0] = parseInt($(this).find(".rating1").text());
               grade_data[subjectTitle][1] = parseInt($(this).find(".rating2").text());
               grade_data[subjectTitle][2] = parseInt($(this).find(".rating3").text());
               grade_data[subjectTitle][3] = parseInt($(this).find(".rating4").text());
               grade_data[subjectTitle][4] = $(this).find(".remarks").text();
         });

      

        grade_data["learning_areas1"] = new Array(5);
        $(parent).find(".learning_areas1").find('.value').each(function(index){
            let val = $(this).text();
            if($(this).hasClass("grade"))
                val = parseInt($(this).text());
            grade_data["learning_areas1"][index] = val;
        });

        grade_data["learning_areas2"] = new Array(5);
        $(parent).find(".learning_areas2").find('.value').each(function(index){
            let val = $(this).text();
            if($(this).hasClass("grade"))
                val = parseInt($(this).text());
            grade_data["learning_areas2"][index] = val;
        });

      }

      for(var k=0; k<core_values.length; k++)
      {
         let arr = grade_data[core_values[k].toLowerCase()] = new Array(4);
         let value_rows1 = $($(".value-row").get(k)).find(".observed_inputs");
         for(var h=0;h<4;h++)
         {
            let new_index = h+(4 * i);
            arr[h] = $(value_rows1.get(new_index)).text().replace('\n', '').trim();
         }
      }
        
         grade_data['card_school_year'] = $($(".card_school_year").get(i)).val();
         grade_data['card_school'] = $($(".card_school").get(i)).val();

         grade_data['eligible_for_admission'] = $($(".eligible_for_admission").get(i)).val();

        sRef[i] = db.collection('grade' + i).doc(key_editing);
        batch.set(sRef[i], grade_data);
        

        //save attendance
        aRef[i] = db.collection('attendance' + i).doc(key_editing);
         let row = $(".attendance_row").get(i);
         let tds = $(row).find(".divInputs");

         let var_school_days = $(tds.get(0)).text();
         if(isNaN(var_school_days))
            var_school_days = 0;
         let var_school_days_absent = $(tds.get(1)).text();
         if(isNaN(var_school_days_absent))
            var_school_days_absent = 0;

         let var_school_tardy = $(tds.get(3)).text();
         if(isNaN(var_school_tardy))
            var_school_tardy = 0;

         let var_school_days_present = $(tds.get(5)).text();
         if(isNaN(var_school_days_present))
            var_school_days_present = 0;

        attendance_data = 
        {
           school_days : parseInt(var_school_days),
           school_days_absent : parseInt(var_school_days_absent),
           cause1 : $(tds.get(2)).text(),
           school_tardy : parseInt(var_school_tardy),
           cause2 : $(tds.get(4)).text(),
           school_days_present : parseInt(var_school_days_present)
        }

        batch.set(aRef[i], attendance_data);
		
		for(var z=0; z<subjects_to_remove.length; z++)
		{
			removeRef[i] = db.collection('grade' + i).doc(key_editing);
			//const removeFruit = removeRef.update();
			var obj = {};
			obj[subjects_to_remove[z]] = firebase.firestore.FieldValue.delete();
			batch.update(removeRef[i], obj);
		}
        

        if(i >= num_grade_card-1)
        {
            return batch.commit().then(function () {
                showLoading(false);
                showNotifSuccess("Saved", "Record saved successfully.");
				
				addGradeLog('Student #' + $("#student_lrn").val() + " record updated by " + $("#user_email").text(), "default")
            });
        }
    }
}

 function imageIsLoaded() { 
   alert(this.src);  // blob url
   // update width and height ...
 }

 function readURL(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     
     reader.onload = function(e) {
       $('#signature_image').attr('src', e.target.result);
     }
     
     reader.readAsDataURL(input.files[0]); // convert to base64 string
   }
 }
 
function startPrint()
{
   var mywindow = window.open('', 'PRINT', 'height=400,width=600');
   let elem = "container_form137";
   var x = document.getElementById("container_form137");
   if(window.getComputedStyle(x).display === "none" )
      elem = "container_sf10";

   // printJS({printable:elem, 
   //    type:'html', 
   // targetStyles: ['*'], 
   // maxWidth : 4280, 
   // showModal: true});

   mywindow.document.write('<html><head><title>' + document.title  + '</title>');

   mywindow.document.write('<meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0">');
   mywindow.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">');
   mywindow.document.write('<link rel="stylesheet" href="css/generic.css">');
   mywindow.document.write('<link rel="stylesheet" href="css/records.css">');
   mywindow.document.write('</head><body >');
   // mywindow.document.write('<h1>' + document.title  + '</h1>');
   let dom_elem = $("#"+elem).clone(true);

   console.log(dom_elem);
   let inputs = $(dom_elem).find("input");

   for (var i = 0; i < inputs.length; i++) {
      if(!$(inputs[i]).is(':checkbox'))
      {
         let text = document.createElement('p');
         text.innerText = inputs[i].value;
         $(text).addClass("underlined");
         inputs[i].parentNode.replaceChild(text, inputs[i]);
      }
      else
      {
         let chk = document.createElement('input');
         chk.type = "checkbox";
         let val = (inputs[i].checked);
         console.log(val);
         $(chk).attr("checked", val);
         inputs[i].parentNode.replaceChild(chk, inputs[i]);
         
      }
   }

   let selects = $(dom_elem).find("select");
   for (var i = 0; i < selects.length; i++) {
      let text = document.createElement('p');
      text.innerText = selects[i].value;
      $(text).addClass("underlined");
      selects[i].parentNode.replaceChild(text, selects[i]);
   }


   let complete_elem = $(dom_elem).get(0).innerHTML;
   // console.log(complete_elem);
   mywindow.document.write(complete_elem);
   mywindow.document.write('</body></html>');

   mywindow.document.close(); // necessary for IE >= 10
   // mywindow.focus(); // necessary for IE >= 10*/

   // mywindow.print();

   mywindow.onload=function(){ // necessary if the div contain images

      mywindow.focus(); // necessary for IE >= 10
      mywindow.print();
      // mywindow.close();
  };
   // mywindow.close();

   return true;
}

function addButtonsCallback()
{
   $("#print_button").on("click", function(){
      startPrint();
   });

   $(".card_school_year, .card_school_yearA").on("change paste keyup", function(){
      let index = $(this).parent().parent().parent().parent().attr("data-index");
      $($(".sy").get(index-1)).text($(this).val());
   });

   $("#signature_input").on( "change", function() {
      readURL(this);
   });

   addPastePrevent($(".observed_inputs"));
   settings = {
      maxLen: 2,
    }

    keys = {
      'backspace': 8,
      'shift': 16,
      'ctrl': 17,
      'alt': 18,
      'delete': 46,
      // 'cmd':
      'leftArrow': 37,
      'upArrow': 38,
      'rightArrow': 39,
      'downArrow': 40,
    }

   utils = {
      special: {},
      navigational: {},
      isSpecial(e) {
        return typeof this.special[e.keyCode] !== 'undefined';
      },
      isNavigational(e) {
        return typeof this.navigational[e.keyCode] !== 'undefined';
      }
   }

    utils.special[keys['backspace']] = true;
    utils.special[keys['shift']] = true;
    utils.special[keys['ctrl']] = true;
    utils.special[keys['alt']] = true;
    utils.special[keys['delete']] = true;

    utils.navigational[keys['upArrow']] = true;
    utils.navigational[keys['downArrow']] = true;
    utils.navigational[keys['leftArrow']] = true;
    utils.navigational[keys['rightArrow']] = true;

    $('.observed_inputs').on('keydown', function(event) {
      let len = event.target.innerText.trim().length;
      hasSelection = false;
      selection = window.getSelection();
      isSpecial = utils.isSpecial(event);
      isNavigational = utils.isNavigational(event);
      
      if (selection) {
        hasSelection = !!selection.toString();
      }
      
      if (isSpecial || isNavigational) {
        return true;
      }
      
      if (len >= settings.maxLen && !hasSelection) {
        event.preventDefault();
        return false;
      }
      
    });

   $(".hasClone").on("change paste keyup", function() {
      let id = $(this).get(0).id;

      if(id.includes('2'))
      {
         $("#"+id.split('2')[0]).val($(this).val());
      }
      else
      {
         $("#"+id+"2").val($(this).val());
      }      
   });

   $(".hasCloneInput").on("change paste keyup", function() {
      let id = $(this).get(0).id;

      let mainParent = $(this).parent().parent().parent().parent();
      let index = mainParent.attr("data-index");
      if(id.charAt(id.length-1) == 'B')
      {
         $("#card" + index).find("#"+id.substring(0, id.length-1) + "A").val($(this).val());
      }
      else
      {
        $("#card"+index+"B").find("#"+id.substring(0, id.length-1) + "B").val($(this).val());
      }      
   });

   $("#button_sf10").on('click', function(){
      $("#container_form137").hide();
      $("#container_sf10").show();      
   });

   $("#button_form137").on('click', function(){
      $("#container_sf10").hide();
      $("#container_form137").show();
   });

    $("#save_button").on('click', function(){
        save();
    });

    $(window).resize(function(){
        // resizeColumnTexts();
    });

    $('#date_of_birth').datepicker({
        autoclose:true,
        endDate:'-7y',
        disableTouchKeyboard: true
    });
    $('#date_of_birth').val("").datepicker("update");
    // $('#date_of_birth').datepicker('update', new Date());

    $('.conducted_from').datepicker({
        autoclose:true,
        disableTouchKeyboard: true
    });
    $('#date_assessment2').datepicker({
        autoclose:true,
        disableTouchKeyboard: true
    });
    
    $('#date_designation').datepicker({
        autoclose:true,
        disableTouchKeyboard: true
    });

    $('#date_designation').val("").datepicker("update");
    // $('#conducted_from').datepicker('update', new Date());

    $('.conducted_to').datepicker({
        autoclose:true,
        disableTouchKeyboard: true
    });
    $('.conducted_to').val("").datepicker("update");

    // $('#conducted_to').datepicker('update', new Date());

    $('#date_assessment').datepicker({
        autoclose:true,
        disableTouchKeyboard: true
    });
    $('#date_assessment').val("").datepicker("update");

    // $('#date_assessment').datepicker('update', new Date());

    //disable right click on amount input
	$('.grade').bind('contextmenu', function (e) {
		return false;
	});

	//disable pasting on amount input
	$('.grade').on('paste', function (e) {
		e.preventDefault();
		return false;
    });
    
    $('body').on('DOMSubtreeModified', '.grade', function(){
		//computeSum($(this));
    });
    
    // add bill form restrict numeric only input
	$('.grade, .inputNumericOnlyDiv').on('keypress', function (event) {

		if ((event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190 || event.keyCode == 110) {
			// if(event.keyCode != 46 && event.keyCode != 8)
			// 	event.preventDefault();

			if(event.keyCode == 190 || event.keyCode == 110){
				let regex = /\d*\.?\d?/g;
				let val = $(this).text();
				$(this).text(regex.exec(val));
			}
            // if ($(this).text().trim() == '')
            //     $(this).text(0);
            // else
                removeLeadingZero($(this));

			return true;
		} 
		else {
			return false;
		}
   });
   
   $('.grade_form2').on('input', ".grade", function() {
	   computeSum(this);
   });
   
   
   $('.grade_form2').on('click', ".delete", function() {
	   var parent = $(this).parent().parent();
	   var name = parent.attr('data-name');
	   var myKey = parent.attr('data-key');
	   
	   bootbox.confirm({
			message: "Are you sure you want to delete the subject " + name + "?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				console.log('This was logged in the callback: ' + result);
				if(result)
				{
					subjects_to_remove.push(myKey);
					$(".subject[data-key='"+myKey+"']").remove();
					//parent.remove();
				}
			}
		});
   });
   
   $('.grade_form2').on('click', ".edit", function() {
	   var parent = $(this).parent().parent();
	   var key = parent.attr('data-key');
	   var index = subjects_to_save.indexOf(key);
	   var prevName = parent.attr('data-name').replace("Subject", "");
	   bootbox.prompt({
			title: "Rename subject : " + prevName, 
			centerVertical: true,
			callback: function(result){
				if(result == null)
					return;
				result = result.trim();
				if(result.includes('_') || result.length == 0)
				{
					bootbox.alert("Invalid subject name should not contain an underscore or is not empty!");
				}
				else
				{
					var newKey = "subject_" + result.trim().replace(/\s\s+/g, ' ').toLowerCase();
					subjects_to_save[index] = newKey;
					
					var name = toTitleCase(newKey.split("_").join(" ")).replace("Subject", "");
					
					$(".subject[data-key='"+key+"']").each(function(){
						$(this).attr('data-key', newKey);
						$(this).attr('data-name', name);
						$(this).find('.card_subject_text').text(name);
					});
					
					if(key != newKey)
					{
						if(subjects_to_remove.indexOf(key) == -1)
						{
							subjects_to_remove.push(key);
						}
					}
				}
				
				console.log(result); 
			}
		});
	   
   });
   
   $('#add_subject_button').on('click', function(){
      bootbox.prompt({
			title: "Add new subject", 
			centerVertical: true,
			callback: function(result){
				if(result == null)
					return;
				result = result.trim();
				if(result.includes('_') || result.length == 0)
				{
					bootbox.alert("Invalid subject name should not contain an underscore or is not empty!");
				}
				else
				{					
					var newKey = "subject_" + result.trim().replace(/\s\s+/g, ' ').toLowerCase();
					
					//Check if subject exist
					if(subjects_to_save.indexOf(newKey) > -1)
					{
						bootbox.alert("Subject already exist!");
						return;
					}
					let data = {};
					data[newKey] = new Array(5);
					data[newKey][0] = 75;
					data[newKey][1] = 75;
					data[newKey][2] = 75;
					data[newKey][3] = 75;					
					createSubjectRow(newKey,data);
					let index = subjects_to_remove.indexOf(newKey);
					if(index > -1)
					{
						subjects_to_remove.splice(index, 1);
					}
				}
			}
	  });
   });
   
   
   /* $(".grade").on('input', function(){
      computeSum(this);
   }); */
         

}

function computeSum(elem)
{
   let parent = $(elem).parent();
   let grade1 = parseInt(parent.find(".rating1").text());
   let grade2 = parseInt(parent.find(".rating2").text());
   let grade3 = parseInt(parent.find(".rating3").text());
   let grade4 = parseInt(parent.find(".rating4").text());

   if(isNaN(grade1))
      grade1 = 0;
   if(isNaN(grade2))
      grade2 = 0;
   if(isNaN(grade3))
      grade3 = 0;
   if(isNaN(grade4))
      grade4 = 0;
   
   let final_rating = (grade1 + grade2 + grade3 + grade4)/4;
   parent.find('.final_rating').text(final_rating);
   let remarks_text = "Failed";
   if(final_rating >= 75)
      remarks_text = "Passed";
   
      // console.log(grade1 +", " + grade2+", " +grade3+", " +grade4);

   parent.find(".remarks").text(remarks_text);

   // compute general average
   for(var i=0;i<num_grade_card;i++)
   {
      var parentA = $("#card"+(i+1));

      var parentB = $("#card"+(i+1+"B"));
      let general_average = new Array(4);
      general_average[0] = 0;
      general_average[1] = 0;
      general_average[2] = 0;
      general_average[3] = 0;
      let subjects = $(parentA).find(".subject");
      subjects.each(function( index ) {
         // let subjectTitle = $(this).get(0).id;

         // if(subjectTitle in data)
         // {
               let val1 = parseInt($(this).find(".rating1").text());
               let val2 = parseInt($(this).find(".rating2").text());
               let val3 = parseInt($(this).find(".rating3").text());
               let val4 = parseInt($(this).find(".rating4").text());

               if(isNaN(val1))
                  val1 = 0;
               if(isNaN(val2))
                  val2 = 0;
               if(isNaN(val3))
                  val3 = 0;
               if(isNaN(val4))
                  val4 = 0;
               general_average[0] += val1;
               general_average[1] += val2;
               general_average[2] += val3;
               general_average[3] += val4;

               // console.log(val1);
         // }
      });

      // console.log(general_average);
      let final1 = Math.round(general_average[0]/subjects.length);
      let final2 = Math.round(general_average[1]/subjects.length);
      let final3 = Math.round(general_average[2]/subjects.length);
      let final4 = Math.round(general_average[3]/subjects.length);
      $($(parentA).find("#subject_general_average").find(".rating1").get(0)).text(final1);
      $($(parentA).find("#subject_general_average").find(".rating2").get(0)).text(final2);
      $($(parentA).find("#subject_general_average").find(".rating3").get(0)).text(final3);
      $($(parentA).find("#subject_general_average").find(".rating4").get(0)).text(final4);

      let final_general_rating = (final1 + final2 + final3 + final4)/4;
      $(parentA).find("#subject_general_average").find('.final_rating').text(final_general_rating);
      let remarks_text = "Failed";
      if(final_general_rating >= 75)
         remarks_text = "Passed";

      $(parentA).find("#subject_general_average").find(".remarks").text(remarks_text);

      //Form 137

      general_average[0] = 0;
      general_average[1] = 0;
      general_average[2] = 0;
      general_average[3] = 0;
      let subjectsB = $(parentB).find(".subject");
      subjectsB.each(function( index ) {
         // let subjectTitle = $(this).get(0).id;

         // if(subjectTitle in data)
         // {
               let val1 = parseInt($(this).find(".rating1").text());
               let val2 = parseInt($(this).find(".rating2").text());
               let val3 = parseInt($(this).find(".rating3").text());
               let val4 = parseInt($(this).find(".rating4").text());

               if(isNaN(val1))
                  val1 = 0;
               if(isNaN(val2))
                  val2 = 0;
               if(isNaN(val3))
                  val3 = 0;
               if(isNaN(val4))
                  val4 = 0;
               general_average[0] += val1;
               general_average[1] += val2;
               general_average[2] += val3;
               general_average[3] += val4;

               // console.log(val1);
         // }
      });

      // console.log(general_average);
      let final1B = Math.round(general_average[0]/subjectsB.length);
      let final2B = Math.round(general_average[1]/subjectsB.length);
      let final3B = Math.round(general_average[2]/subjectsB.length);
      let final4B = Math.round(general_average[3]/subjectsB.length);
      $($(parentB).find("#subject_general_average").find(".rating1").get(0)).text(final1B);
      $($(parentB).find("#subject_general_average").find(".rating2").get(0)).text(final2B);
      $($(parentB).find("#subject_general_average").find(".rating3").get(0)).text(final3B);
      $($(parentB).find("#subject_general_average").find(".rating4").get(0)).text(final4B);

      let final_general_ratingB = (final1B + final2B + final3B + final4B)/4;
      $(parentB).find("#subject_general_average").find('.final_rating').text(final_general_ratingB);
      let remarks_textB = "Failed";
      if(final_general_ratingB >= 75)
         remarks_textB = "Passed";

      $(parentB).find("#subject_general_average").find(".remarks").text(remarks_textB);

   }

   
}

function finalComputeSum()
{

}

function fetchStudentRecord(key)
{
    showLoading(true, "Loading student record...");
    try {

        db.collection("students").doc(key)
        .get().then(function(doc) {
            if (doc.exists) {
                let data = doc.data();

                showLoading(false);

                setStudentRecord(data);
            } else {

                bootbox.alert({
                    centerVertical:true,
                    size:'small',
                    message: "No such document!"
                });

            }
        }).catch(function(error) {
            bootbox.alert({
                centerVertical:true,
                size:'small',
                message:error.message
            });
        });
    }
    catch(error){
        bootbox.alert({
            centerVertical:true,
            size:'small',
            message:error.message
        });
    }
}

function setStudentRecord(data)
{
    // $("form").get(0).reset();
    $( "form" ).each(function( index ) {
        $( this ).get(0).reset();
    });

    $( 'input[type="checkbox"]' ).prop('checked', false);


    $("#student_lrn").val(data.LRN);
    $("#first_name").val(toTitleCase(data.first_name));
    $("#name_ext_name").val(toTitleCase(data.name_ext_name));
    $("#middle_name").val(toTitleCase(data.middle_name));
    $("#last_name").val(toTitleCase(data.last_name));
    $("#gender").val(data.gender);
    $("#date_of_birth").datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    }).datepicker("update", data.birthday.toDate()); 

    //for form137
    $("#student_lrn2").val(data.LRN);
    $("#first_name2").val(toTitleCase(data.first_name));
    $("#name_ext_name2").val(toTitleCase(data.name_ext_name));
    $("#middle_name2").val(toTitleCase(data.middle_name));
    $("#last_name2").val(toTitleCase(data.last_name));
    $("#gender2").val(data.gender);
    $("#date_of_birth2").datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    }).datepicker("update", data.birthday.toDate()); 

    if("address_of_school" in data)
        $("#address_of_school2").val(data.address_of_school);

    if("guardian" in data)
      $("#guardian").val(data.guardian);

   if("guardian_address" in data)
      $("#guardian_address").val(data.guardian_address);

   if("guardian_occupation" in data)
      $("#guardian_occupation").val(data.guardian_occupation);

    if("kinder_progress_report" in data)
        $('#kinder_progress_report').prop('checked', data.kinder_progress_report);
    
    if("eccd_checklist" in data)
        $("#eccd_checklist").prop('checked', data.eccd_checklist);
    if("kinder_certificate_completion" in data)
        $("#kinder_certificate_completion").prop('checked', data.kinder_certificate_completion);
    
    if("name_of_school" in data)
        $("#name_of_school").val(data.name_of_school);
    if("school_id" in data)
        $("#school_id").val(data.school_id);
    if("address_of_school" in data)
        $("#address_of_school").val(data.address_of_school);

    if("pept_passer_rating" in data)
        $("#pept_passer_rating").val(data.pept_passer_rating);
    if("date_assessment" in data){
        if(data.date_assessment != null)
        {
            $("#date_assessment").datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            }).datepicker("update", data.date_assessment.toDate()); 

            $("#date_assessment2").datepicker({
               format: 'mm/dd/yyyy',
               autoclose: true
           }).datepicker("update", data.date_assessment.toDate()); 
        }
    }
            
    if("others_eligibility" in data)
        $("#others_eligibility").val(data.others_eligibility);
    if("testing_center" in data)
        $("#testing_center").val(data.testing_center);    
    if("remark_eligibility" in data)
        $("#remark_eligibility").val(data.remark_eligibility);

    //certificate of transfer
    $("#record_of").text(data.record_of);
    $("#he_she_eligible").text(data.he_she_eligible);

    for(var i=0; i < num_grade_card; i++)
    {
        fetchGrades(i);
        fetchAttendance(i);
    }
}

function fetchAttendance(i)
{
   let table = $("#attendance_table");
   db.collection("attendance"+i).doc(key_editing)
   .get().then(function(doc) {
         if (doc.exists) {
            let data = doc.data();

            // console.log(data);
            let grade_row = $(table).find(".attendance_row").get(i);
            // .each(function(){
               let rows = $(grade_row).find(".divInputs");
               let var_school_days = "";
               if(!isNaN(data.school_days))
                  var_school_days = data.school_days;
               $(rows.get(0)).text(var_school_days);

               let var_school_days_absent = "";
               if(!isNaN(data.school_days_absent))
                  var_school_days_absent = data.school_days_absent;
               $(rows.get(1)).text(var_school_days_absent);
               $(rows.get(2)).text(data.cause1);

               let var_school_tardy = "";
               if(!isNaN(data.school_tardy))
                  var_school_tardy = data.school_tardy;
               $(rows.get(3)).text(var_school_tardy);
               $(rows.get(4)).text(data.cause2);

               let var_school_days_present = "";
               if(!isNaN(data.school_days_present))
                  var_school_days_present = data.school_days_present;
               $(rows.get(5)).text(var_school_days_present);
            // });
         }
   }).catch(function(error) {
      showNotifError("Error", error.message);
   });
}

function fetchGrades(i)
{
    
   var parent = $("#card"+(i+1));//$("#card_container").find(".grade_card")[i];
   var parentB = $("#card"+(i+1+"B"));
   

   db.collection("grade"+i).doc(key_editing)
   .get().then(function(doc) {
         if (doc.exists) {
            // console.log("Document data:", doc.data());
            let data = doc.data();

         //   console.log(parent);
            $($(".card_school").get(i)).val(data.card_school);
            $($(".card_schoolA").get(i)).val(data.card_school);
            $($(".eligible_for_admission").get(i)).val(data.eligible_for_admission);
            
            $(parent).find("#card_school_id").val(data.card_school_id);
            $(parent).find("#card_district").val(data.card_district);
            $(parent).find("#card_division").val(data.card_division);
            $(parent).find("#card_region").val(data.card_region);
            $(parent).find("#card_classified").val(data.card_classified);
            $(parent).find("#card_section").val(data.card_section);
            $($(".card_school_year").get(i)).val(data.card_school_year);
            $($(".card_school_yearA").get(i)).val(data.card_school_year);
            $($(".sy").get(i)).text(data.card_school_year);
            $(parent).find("#card_adviser").val(data.card_adviser);
            // $(parent).find("#card_signature").val(data.card_signature);

			var pushedKey = new Array();
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					
					
					if(Array.isArray(data[key]) && !(core_values.includes(key)) && key.includes("subject_") && !(fixed_subjects.includes(key)))
					{
						//console.log(key + " is an array");
						createSubjectRow(key, data);
						//if(!pushedKey.includes(key))
//							console.log('"' + key + '" :new Array(5),');
					}
					
					if(Array.isArray(data[key])){
						if(!pushedKey.includes(key) && !key.includes("-")){
							if(core_values.includes(jsUcfirst(key)))
								console.log('"' + key + '" :[0,0,0,0],');
							else
								console.log('"' + key + '" :[0,0,0,0,0],');
							pushedKey.push(key);
						}
					}
					else{
						if(!pushedKey.includes(key)){
							console.log('"' + key + '" :"",');
							pushedKey.push(key);
						}
					}
				}
			}

            /* $(parent).find(".subject").each(function( index ) {
               let subjectTitle = $(this).attr("data-key");
			   
			   console.log(subjectTitle);

               if(subjectTitle in data)
               {
                     let val1 = data[subjectTitle][0];
                     let val2 = data[subjectTitle][1];
                     let val3 = data[subjectTitle][2];
                     let val4 = data[subjectTitle][3];

                     $(this).find(".rating1").text(Number.isNaN(val1)? "": val1);
                     $(this).find(".rating2").text(Number.isNaN(val2)? "": val2);
                     $(this).find(".rating3").text(Number.isNaN(val3)? "": val3);
                     $(this).find(".rating4").text(Number.isNaN(val4)? "": val4);

                     computeSum($(this).find(".rating4"));    
               }
            }); */


            $(parentB).find(".subject").each(function( index ) {
				let subjectTitle = $(this).get(0).id;

				if(subjectTitle in data)
				{
					  let val1 = data[subjectTitle][0];
					  let val2 = data[subjectTitle][1];
					  let val3 = data[subjectTitle][2];
					  let val4 = data[subjectTitle][3];

					  $(this).find(".rating1").text(Number.isNaN(val1)? "": val1);
					  $(this).find(".rating2").text(Number.isNaN(val2)? "": val2);
					  $(this).find(".rating3").text(Number.isNaN(val3)? "": val3);
					  $(this).find(".rating4").text(Number.isNaN(val4)? "": val4);

					  computeSum($(this).find(".rating4"));    
				}
			 });


            //For form137

            $("#observed_table").find(".value-row").each(function(index){
            let values_key = core_values[index].toLowerCase();
            let start_index = i*4;
               if(values_key in data)
               {                       
               var myInputs = $(this).find(".observed_inputs");
               
               let array = data[values_key];
               for(var z=0;z<array.length;z++)
               {
                  $(myInputs.get(z+start_index)).html(array[z]);
               }
            }
            });
            
            if(data.conducted_from != null)
            {
               $(parent).find(".conducted_from").datepicker({
                     format: 'dd/mm/yyyy',
                     autoclose: true
               }).datepicker("update", data.conducted_from.toDate());
            }
            

            if(data.conducted_to != null)
            {
               $(parent).find(".conducted_to").datepicker({
                     format: 'dd/mm/yyyy',
                     autoclose: true
               }).datepicker("update", data.conducted_to.toDate()); 
            }
            
            if("learning_areas1" in data)
            {
               $(parent).find(".learning_areas1").find('.value').each(function(index){
                     if($(this).hasClass("grade"))
                     {
                        let val = data.learning_areas1[index];
                        $(this).text(Number.isNaN(val)? "": val );
                     }
                     else
                        $(this).text(data.learning_areas1[index]);
               });
            }

            if("learning_areas2" in data)
            {
               $(parent).find(".learning_areas2").find('.value').each(function(index){
                     if($(this).hasClass("grade"))
                     {
                        let val = data.learning_areas2[index];
                        $(this).text(Number.isNaN(val)? "": val );
                     }
                     else
                        $(this).text(data.learning_areas2[index]);
               });
            }

         } else {
            // doc.data() will be undefined in this case
            console.log("No grade" + i);
         }
   }).catch(function(error) {
         showNotifError("Error", error.message);
   });
}


function createSubjectRow(key,data)
{
	$('.grade_forms').each(function(){
		if($(this).find(".subject[data-key='" + key + "']").length == 0)
		{
			var afterGeneralSubject = $(this).find('.general_average');
			var inserted = $(grade_row_template).insertAfter(afterGeneralSubject);
			inserted.attr('data-key', key);
			var name = toTitleCase(key.split("_").join(" ")).replace("Subject", "");
			inserted.attr('data-name', name);
			inserted.find('.card_subject_text').text(name);
			
			if(subjects_to_save.indexOf(key) == -1)
				subjects_to_save.push(key);
			
			var subjectTitle = key;

			let val1 = data[subjectTitle][0];
			let val2 = data[subjectTitle][1];
			let val3 = data[subjectTitle][2];
			let val4 = data[subjectTitle][3];

			inserted.find(".rating1").text(Number.isNaN(val1)? "": val1);
			inserted.find(".rating2").text(Number.isNaN(val2)? "": val2);
			inserted.find(".rating3").text(Number.isNaN(val3)? "": val3);
			inserted.find(".rating4").text(Number.isNaN(val4)? "": val4);

			computeSum(inserted.find(".rating4").get(0)); 
		}
	});
}

//<a class='delete' href="#">delete...</a>

var grade_row_template =   

  `<div class="form-row subject">

     <div class="col-4 cell-bordered card_subject_text_parent">
        <p class="text-left card_subject_text"></p>
		
		<button class='delete btn btn-danger pull-right' type='button'>
        Delete
		</button>
		<button class='edit btn btn-primary pull-left' type='button'>
        Edit
		</button>
     </div>

     <div class="col-1 cell-bordered divInputs grade py-2 rating1" contenteditable="true">
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating2" contenteditable="true">
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating3" contenteditable="true">
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating4" contenteditable="true">
     </div>

     <div class="col-2 grid_cell cell-bordered final_rating">
     </div>

     <div class="col-2 grid_cell cell-bordered remarks">
     </div>

  </div>`;


var card_template2 = 
`
<form class="form-entry grade_form2 grade_forms">
<div class="form-row">
   <div class="form-group col-8">
      <label class="control-label" >School:</label>
      <input type="text" class="form-control card_school hasCloneInput" id="card_schoolB" >
   </div>

   <div class="form-group col-4">
        <label class="control-label" >School Year:</label>
        <input type="text" class="form-control inputNumericOnlyWithDash card_school_year hasCloneInput" id="card_school_yearB">
     </div>
</div>

<div class="form-row">

     <div class="col-4 grid_cell cell-bordered">
        <h4 class="all_caps text-center card_subject_text">Learning Areas</h4>
     </div>

     <div class="col-4 cell-bordered-header">
        <div class="row grid_cell">
           <p>Periodic Rating</p>
        </div>

        <div class="row cell-bordered-header">
           <div class="col cell-bordered-header">
              <p class="text-center">1</p>
           </div>
           <div class="col cell-bordered-header">
              <p class="text-center">2</p>
           </div>
           <div class="col cell-bordered-header">
              <p class="text-center">3</p>
           </div>
           <div class="col cell-bordered-header">
              <p class="text-center">4</p>
           </div>
        </div>
     </div>

     <div class="col-2 grid_cell cell-bordered">
        <div class="row grid_cell">
           <p class="card_subject_text">Final Rating</p>
        </div>
     </div>

     <div class="col-2 grid_cell cell-bordered">
        <div class="row grid_cell">
           <p class="card_subject_text">Remarks</p>
        </div>
     </div>

  </div>
  
  



   <!-- General Average -->
   <div class="form-row subject general_average" id="subject_general_average">

     <div class="col-4 cell-bordered py-2">
        General Average
     </div>

     <div class="col-1 cell-bordered divInputs grade py-2 rating1" >
        &nbsp;
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating2">
        &nbsp;
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating3">
        &nbsp;
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating4" >
        &nbsp;
     </div>

     <div class="col-2 grid_cell cell-bordered final_rating">
     </div>

     <div class="col-2 grid_cell cell-bordered divInputs py-2 remarks">
        &nbsp;
     </div>

  </div>

  <div class="form-row">
   <div class="form-group col-8">
      <label class="control-label" for="eligible_admission">Eligible for Admission to Grade:</label>
      <input type="text" class="form-control eligible_for_admission" id="eligible_admission">
   </div>
</div>

</form>
   
   `

function asyncFunction (item, cb) {
    setTimeout(() => {
        
      cb();
    }, 100);
  }

  var card_template = 
  `<form class="form-entry grade_form grade_forms">
  <div class="form-row">
     <div class="form-group col-8">
        <label class="control-label" >School:</label>
        <input type="text" class="form-control card_schoolA hasCloneInput" id="card_schoolA">
     </div>

     <div class="form-group col-4">
        <label class="control-label" for="card_school_id">School ID:</label>
        <input type="text" class="form-control" id="card_school_id">
     </div>
  </div>

  <div class="form-row">
     <div class="form-group col-3">
        <label class="control-label" for="card_district">District:</label>
        <input type="text" class="form-control" id="card_district">
     </div>

     <div class="form-group col-6">
        <label class="control-label" for="card_division">Division:</label>
        <input type="text" class="form-control" id="card_division">
     </div>

     <div class="form-group col-3">
        <label class="control-label" for="card_region">Region:</label>
        <input type="text" class="form-control" id="card_region">
     </div>

  </div>

  <div class="form-row">
     <div class="form-group col-4">
        <label class="control-label" for="card_classified">Classified as Grade:</label>
        <input type="text" class="form-control" id="card_classified">
     </div>

     <div class="form-group col-4">
        <label class="control-label" for="card_section">Section:</label>
        <input type="text" class="form-control" id="card_section">
     </div>

     <div class="form-group col-4">
        <label class="control-label" >School Year:</label>
        <input type="text" class="form-control inputNumericOnlyWithDash card_school_yearA hasCloneInput" id="card_school_yearA">
     </div>

  </div>

  <div class="form-row">
     <div class="form-group col-6">
        <label class="control-label" for="card_adviser">Name of Adviser/Teacher:</label>
        <input type="text" class="form-control" id="card_adviser">
     </div>

     <div class="form-group col-6">
        <label class="control-label" for="card_signature">Signature:</label>
        <input type="text" class="form-control" id="card_signature" disabled="true">
     </div>

  </div>

  <div class="form-row">

     <div class="col-4 grid_cell cell-bordered">
        <h4 class="all_caps text-center card_subject_text">Learning Areas</h4>
     </div>

     <div class="col-4 cell-bordered-header">
        <div class="row grid_cell">
           <p>Quarterly Rating</p>
        </div>

        <div class="row cell-bordered-header">
           <div class="col cell-bordered-header">
              <p class="text-center">1</p>
           </div>
           <div class="col cell-bordered-header">
              <p class="text-center">2</p>
           </div>
           <div class="col cell-bordered-header">
              <p class="text-center">3</p>
           </div>
           <div class="col cell-bordered-header">
              <p class="text-center">4</p>
           </div>
        </div>
     </div>

     <div class="col-2 grid_cell cell-bordered">
        <div class="row grid_cell">
           <p class="card_subject_text">Final Rating</p>
        </div>
     </div>

     <div class="col-2 grid_cell cell-bordered">
        <div class="row grid_cell">
           <p class="card_subject_text">Remarks</p>
        </div>
     </div>

  </div>
  
  


   <!-- General Average -->
   <div class="form-row subject general_average" id="subject_general_average">

     <div class="col-4 cell-bordered py-2">
        General Average
     </div>

     <div class="col-1 cell-bordered divInputs grade py-2 rating1" >
        &nbsp;
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating2" >
        &nbsp;
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating3" >
        &nbsp;
     </div>
     <div class="col-1 cell-bordered divInputs grade py-2 rating4" >
        &nbsp;
     </div>

     <div class="col-2 grid_cell cell-bordered final_rating">
     </div>

     <div class="col-2 grid_cell cell-bordered divInputs py-2 remarks">
        &nbsp;
     </div>

  </div>

  <!-- Remedial start -->
  <div class="form-row">

     <div class="form-group col-4 cell-bordered grid_cell">
        <p>Remedial Classes</p>
     </div>

     <div class="form-group col-4">
        <label for="conducted_from" class="control-label">Conducted from:</label>
        <input id="conducted_from" class="conducted_from datepicker form-control" data-date-format="mm/dd/yyyy" placeholder="Select date">
     </div>

     <div class="form-group col-4">
        <label for="conducted_to" class="control-label">to:</label>
        <input id="conducted_to" class="conducted_to datepicker form-control" data-date-format="mm/dd/yyyy" placeholder="Select date">
     </div>

  </div>

  <!-- Remedial ratings -->
  <div class="form-row">

     <div class="form-group col-4 cell-bordered grid_cell">
        <p class="text-center column-text">Learning Areas</p>
     </div>

     <div class="form-group col-2 cell-bordered grid_cell">
        <p class="text-center column-text">Final Rating</p>
     </div>

     <div class="form-group col-2 cell-bordered grid_cell">
        <p class="text-center column-text">Remedial Class Mark</p>
     </div>

     <div class="form-group col-2 cell-bordered grid_cell">
        <p class="text-center column-text">Recomputed Final Grade</p>
     </div>

     <div class="form-group col-2 cell-bordered grid_cell">
        <p class="text-center column-text">Remarks</p>
     </div>

  </div>

  <!-- Remedial values row 1 -->
  <div class="form-row learning_areas1" id="learning_areas1">

     <div class="form-group col-4 cell-bordered grid_cell py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell grade py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell grade py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell py-2 value" contenteditable="true">
        &nbsp;
     </div>

  </div>

  <!-- Remedial values row 2 -->
  <div class="form-row learning_areas2" id="learning_areas2">

     <div class="form-group col-4 cell-bordered grid_cell py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell py-2 grade value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell grade py-2 value" contenteditable="true">
        &nbsp;
     </div>

     <div class="form-group col-2 cell-bordered grid_cell py-2 value" contenteditable="true">
        &nbsp;
     </div>

  </div>

  </form>`;


  var form137_bottom =
  `


  <p style="margin-bottom: 0.35cm;"><span style="font-family: Cambria, serif;"><span style="font-size: medium;"><strong>REPORT ON LEARNER&rsquo;S OBSERVED VALUES </strong></span></span></p>
  <div class="table-responsive">
   <table class="table table-bordered" id="observed_table_main">
     <tbody id="observed_table" style="border:1px solid black">
        <tr valign="top">
           <td >
              <p style="orphans: 2; widows: 2;" align="center">&nbsp;</p>
           </td>
           <td  colspan="4" width="142">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Gr. 1 S.Y.<p class="underlined_field sy" ></p></strong></span></span></p>
           </td>
           <td  colspan="4" width="142">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Gr. 2 S.Y.<p class="underlined_field sy" ></p></strong></span></span></p>
           </td>
           <td  colspan="4" width="141">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Gr. 3 S.Y.<p class="underlined_field sy" ></p></strong></span></span></p>
           </td>
           <td  colspan="4" width="142">
               <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Gr. 4 S.Y.<p class="underlined_field sy" ></p></strong></span></span></p>
            </td>
            <td  colspan="4" width="142">
               <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Gr. 5 S.Y.<p class="underlined_field sy" ></p></strong></span></span></p>
            </td>
            <td  colspan="4" width="141">
               <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Gr. 6 S.Y.<p class="underlined_field sy" ></p></strong></span></span></p>
            </td>
        </tr>
        <tr>
           <td  rowspan="2" valign="top" width="77">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Core Values</strong></span></span></p>
           </td>
           <td  rowspan="2" width="160">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Behavior Statements</strong></span></span></p>
           </td>
           <td  colspan="4" valign="top" width="142">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Quarter</strong></span></span></p>
           </td>
           <td  colspan="4" valign="top" width="142">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Quarter</strong></span></span></p>
           </td>
           <td  colspan="4" valign="top" width="141">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Quarter</strong></span></span></p>
           </td>
           <td  colspan="4" valign="top" width="142">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Quarter</strong></span></span></p>
           </td>
           <td  colspan="4" valign="top" width="142">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Quarter</strong></span></span></p>
           </td>
           <td  colspan="4" valign="top" width="141">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>Quarter</strong></span></span></p>
           </td>
        </tr>
        <tr valign="top">
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>1</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>2</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>3</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>4</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>1</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>2</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>3</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>4</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>1</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>2</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>3</strong></span></span></p>
           </td>
           <td  width="24">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>4</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>1</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>2</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>3</strong></span></span></p>
           </td>
           <td  width="24">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>4</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>1</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>2</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>3</strong></span></span></p>
           </td>
           <td  width="24">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>4</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>1</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>2</strong></span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>3</strong></span></span></p>
           </td>
           <td  width="24">
              <p style="orphans: 2; widows: 2;" align="center"><span style="font-family: Cambria, serif;"><span style="font-size: small;"><strong>4</strong></span></span></p>
           </td>
        </tr>
        <tr valign="top" class="value-row">
           <td >
               1. Maka-Diyos
              
           </td>
           <td class="tds">
              Expresses one&rsquo;s spiritual beliefs while respecting the spiritual beliefs of others
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
        </tr>
        <tr valign="top" class="value-row">
           <td  width="77" height="27">
              <p style="margin-bottom: 0cm; orphans: 2; widows: 2;" align="center">&nbsp;</p>
              <p style="orphans: 2; widows: 2;" align="left"><span style="font-family: Cambria, serif;"><span style="font-size: small;">2. Makatao</span></span></p>
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="left"><span style="font-family: Cambria, serif;"><span style="font-size: small;">Shows adherence to ethical principles by upholding truth</span></span></p>
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs tds" contenteditable="true">
           </td>

           <td class="divInputs observed_inputs tds"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs tds"  contenteditable="true">
           </td>
        </tr>
        <tr valign="top" class="value-row">
           <td >
              
              3. Maka-kalikasan
              
           </td>
           <td >
              <p style="orphans: 2; widows: 2;" align="left"><span style="font-family: Cambria, serif;"><span style="font-size: small;">Cares for the environment and utilizes resources wisely, judiciously, and economically</span></span></p>
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
        </tr>
        <tr valign="top" class="value-row">
           <td  height="50">
              <p style="margin-bottom: 0cm; orphans: 2; widows: 2;" align="center">&nbsp;</p>
              <p style="margin-bottom: 0cm; orphans: 2; widows: 2;" align="center">&nbsp;</p>
              <p style="margin-bottom: 0cm; orphans: 2; widows: 2;" align="center">&nbsp;</p>
              4.Makabansa
           </td>
           <td class="tds">
              Demonstrates pride in being a Filipino; exercises the rights and responsibi-lities of a Filipino citizen.
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

        </tr>
        <tr valign="top" class="value-row">
           <td class="tds">
              Demonstrates appropriate behavior in carrying out activities in the school, community, and country
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>

           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
           <td class="divInputs observed_inputs"  contenteditable="true">
           </td>
        </tr>
     </tbody>
  </table>
  </div>
  
  <p ><strong>AO</strong> &ndash; Always Observed <strong>SO &ndash;</strong> Sometimes Observed <strong>RO</strong> &ndash; Rarely Observed <strong>NO</strong>&ndash; Not Observed</p>
  <br>
  <br>
  `;

  var part_4 = `
  

  <p><strong>ATTENDANCE RECORD</strong></p>
  <div class="table-responsive">
  <table class="table table-bordered" id="attendance_table">
  <tbody>
  <tr>
  <td>
  <p><strong>Grade</strong></p>
  </td>
  <td>
  <p><strong>No. of School Days</strong></p>
  </td>
  <td>
  <p><strong>No. of School Days Absent</strong></p>
  </td>
  <td>
  <p><strong>Cause</strong></p>
  </td>
  <td>
  <p><strong>No. of Times Tardy</strong></p>
  </td>
  <td>
  <p><strong>Cause</strong></p>
  </td>
  <td>
  <p><strong>No. of School Days Present</strong></p>
  </td>
  </tr>
  <tr class="attendance_row">
  <td>I</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  </tr>
  <tr class="attendance_row">
  <td>II</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  </tr>
  <tr class="attendance_row">
  <td>III</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  </tr>
  <tr class="attendance_row">
  <td>IV</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  </tr>
  <tr class="attendance_row">
  <td>V</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  </tr>
  <tr class="attendance_row">
  <td>VI</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  <td class="divInputs table-word-column" contenteditable="true">&nbsp;</td>
  <td class="divInputs inputNumericOnlyDiv" contenteditable="true">&nbsp;</td>
  </tr>

  </tbody>
  </table>
  </div>
  <p><strong>CERTIFICATE OF TRANSFER</strong></p>
  <p>TO WHOM IT MAY CONCERN:</p>
  <p>This is to certify that this is a true record of the Elementary School Permanent Record of</p>
  <p id="record_of" class="underlined_field toSaveDiv" contenteditable="true"></p>
  <br>
  <p> He/She is eligible for transfer and admission of Grade/Year</p>
  <p id="he_she_eligible" class="underlined_field toSaveDiv" contenteditable="true"></p>
  
  <p>Signature</p>
  <p class="underlined_field" ></p>
   
  <p>Date Designation</p>
  <div class="row">
  <div class="col-4">
  <input id="date_designation" class="datepicker form-control" data-date-format="mm/dd/yyyy" placeholder="Select date" ></input>
  </div>
   </div>
`

{/* <input type='file' id="signature_input" />
  <br>
  <img id="signature_image" src="#" alt="signature image" />
 */}