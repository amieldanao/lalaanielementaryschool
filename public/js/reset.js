$(document).on('click','.pass_show .ptxt', function(){ 

  $(this).text($(this).text() == "Show" ? "Hide" : "Show"); 
  
  $(this).prev().prev().attr('type', function(index, attr){return attr == 'password' ? 'text' : 'password'; }); 
  
});  


$(document).ready( function () {

    $('.pass_show').append('<span class="ptxt">Show</span>');

    var url_string = window.location.href;
    var url = new URL(url_string);
    var code = url.searchParams.get("oobCode");
	
	

    console.log(code);
	
	addPastePrevent($("#field_password"));
	addPastePrevent($("#field_password_confirm"));

	$(".passInput").on("input", function(){
		console.log("changePasswordInput");
		
		var errors = passIsStrong($(this).val());
		
		if(errors.length > 0){
			$(this).next().removeClass("d-none");
			$(this).next().html( errors.join("<br/>"));
		}
		else{
			$(this).next().addClass("d-none");
		}
	}); 
	
	$('.passInput').on('keypress', function(e) {
		if (e.which == 32){
			//console.log('Space Detected');
			return false;
		}
		console.log("changePasswordInput");
		var errors = passIsStrong($(this).val());
		
		if(errors.length > 0){
			$(this).next().removeClass("d-none");
			$(this).next().html(errors.join("<br/>"));
		}
		else{
			$(this).next().addClass("d-none");
		}
	});
	

	if(code == null)
	{
		window.location.replace("index.html");
	}
	else
	{
		firebase.auth().verifyPasswordResetCode(code)
		.then(function(email) {
		  // Display a "new password" form with the user's email address
		  $("form").show();

		  $("#email_to_reset").text(email);

		  $("#reset_btn").on('click', function(){
			  if($("#field_password").val() == $("#field_password_confirm").val())
			  {
				 var errors = passIsStrong($("#field_password").val());
				if(errors.length == 0)
				{
					showLoading(true, "Resetting password...");
					let newPassword = $("#field_password_confirm").val();
					firebase.auth().confirmPasswordReset(code, newPassword)
					.then(function() {
						// Success
						showLoading(false);
						bootbox.alert("Password reset successfully");
						window.location.replace("index.html");
					})
					.catch(function() {
					// Invalid code
					  showLoading(false);
					  console.log("invalid code 1");
					  showUnAuthorized();
						
					});
				}
				
			  }   
			  else
			  {
				bootbox.alert("Password doesn't match!");
			  }         
		  });
		})
		.catch(function(error) {
		  // Invalid code
		  console.log(error);
		  bootbox.alert(error.message);
		  // showUnAuthorized();
		});

		
	
	}
	
	
});

function passIsStrong(newPass)
{
	var retArrayErrors = new Array();
	var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
	if(format.test(newPass) == false)
	{
		retArrayErrors.push("Password must atleast contain 1 special character");
		//return false;	
	}
	
	if(newPass.length < 8)
	{
		retArrayErrors.push("Password must contain atleast 8 characters");
		//return false;	
	}
	
	var regExp = /[a-zA-Z]/g;
	if(regExp.test(newPass) == false)
	{
		retArrayErrors.push("Password must contain a letter character");
		//return false;
	}
	
	if(/\d/.test(newPass) == false)
	{
		retArrayErrors.push("Password must contain a number character");
		//return false
	}
	
	if(newPass == undefined || newPass[0] != newPass[0].toUpperCase()){
		retArrayErrors.push("Password must begin with uppercase letter");
	}
	
	return retArrayErrors;
}

