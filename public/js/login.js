/*jslint indent: 4 */
/*global document, require, console, window*/
/*eslint no-console: 0*/

'use strict';
var usernameInput;
var passwordInput;
// var rootRef;
var signing_in;

$(document).ready(function()                    
{
    firebase.auth().signOut().then(function(result){
        console.log("logged out!");
    });

    initVariables();

    handleLoginForm();

    $("#btn_password_reset").on('click', function(){
        bootbox.prompt({
            title: "Please enter your email account", 
            centerVertical: true,
            callback: function(result){ 
                console.log(result); 
                if(result != null)
                {
                    if(result.trim().length > 0)
                    {
                        showLoading(true, "Sending password reset...");
                        firebase.auth().sendPasswordResetEmail(
                            result, null)
                            .then(function() {
                              // Password reset email sent.
                              showLoading(false);
                              bootbox.alert("Password reset link was sent to your email.");
                            })
                            .catch(function(error) {
                                showLoading(false);
                              // Error occurred. Inspect error.code.
                                bootbox.alert(error.message);
                            });
                    }
                }
            }
        });
    });

});



function initVariables()
{
    $("form").get(0).reset();

    usernameInput = $("#login");
    passwordInput = $("#password");
}

function handleLoginForm()
{
    $('form').bind('submit', $('form'), function(event) {
        var form = this;

        event.preventDefault();
        event.stopPropagation();

        if (form.submitted) {
            return;
        }

        form.submitted = true;

        if(!signing_in)
            login();

    });
}

function login()
{
    signing_in = true;
    showLoading(true, "signing in...");
    $("#logInBtn").prop('disabled', true);
    var email = usernameInput.val().toLowerCase();
    var pword = passwordInput.val();

    try
    {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then(function() 
            {
            firebase_auth.signInWithEmailAndPassword(email, pword).then(function(user){
                // console.log(user);
                if(user)
                {
                    // update password on database
                    try
                    {
                        let query = db.collection('accounts').where('email', '==', email);

                        query.get().then(function(querySnapshot) {

                            if(querySnapshot.size > 0)
                            {
                                if(querySnapshot.docs[0].data().status == "Active")
                                {
                                    let key = querySnapshot.docs[0].id;
									addAccountLog('Account Log in ' + email , "default");
                                    db.collection('accounts').doc(key).set({password:pword}, {merge:true})
                                    .then(function(docRef) {
                                        // showNotifSuccess('Saved', capitalizeFLetter(data.ID) + ' saved successfully');
                                        showLoading(false);
										
                                        window.location = "home.html";
                                    })
                                    .catch(error => {
                                        console.log("error on write");
                                        showLoading(false);
                                        bootbox.alert(error.message);
                                        errorLogin();                         

                                    });
                                }
                                else
                                {
                                    showLoading(false);
                                    bootbox.alert("Your account is not active!");
                                    errorLogin(); 
                                }

                            }
                            else
                            {
                                showLoading(false);
                                bootbox.alert("User doesn't exist!");
                                errorLogin();
                            }
                        })
                        .catch(error => {
                            console.log("error on read");
                            showLoading(false);
                            bootbox.alert(error.message);
                            errorLogin();                     

                        });
                    }
                    catch(error)
                    {
                        console.log("error on try/catch");
                        showLoading(false);
                        bootbox.alert(error.message);
                        errorLogin();
                    }
                }
            }).
            catch(function(error) {

                
                showLoading(false);
                bootbox.alert(error.message);
                errorLogin();
                
            });
        })
        .catch(function(error) {
        errorLogin();
        if(error){
            showLoading(false);
            bootbox.alert(error.message);
            errorLogin();
        }
    });

    
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message + "<br> Try restarting your browser.");
        errorLogin();
    }
}


function errorLogin()
{
    firebase.auth().signOut();
    $("#logInBtn").prop('disabled', false);
    signing_in = false;
    $('form').get(0).submitted = false;
}