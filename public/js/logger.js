

$(document).ready(function()                    
{
	
	$('.input-daterange input').each(function() {
		
		$(this).datepicker({
			autoclose : true
		})
		.datepicker("update",new Date());
		
		$(this).on('changeDate', function(e) {
			// `e` here contains the extra attributes
			let id = $(this).get(0).id;
			if(id == "start_date")
			{
				$('#end_date').datepicker('setStartDate', $("#start_date").val());
			}
			else
			{
				$('#start_date').datepicker('setEndDate', $("#end_date").val());
			}			
		});
	});
	
	/* $("#start_date").datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    })
	.on('changeDate', function(e) {
        // `e` here contains the extra attributes
		console.log(e);
    })
	.datepicker("update",new Date());
	
	$("#end_date").datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    })
	.on('changeDate', function(e) {
        // `e` here contains the extra attributes
		console.log(e);
    })
	.datepicker("update", new Date());  */
	
	let splitted_link = window.location.href.split("/");
    let current_page = splitted_link[splitted_link.length-1];
	if(current_page.replace(/#/g, '') == "logs.html")
	{
		fetchLogs();
	}

	$('#go_submit').on('click', function(){
		fetchLogs();
	});
});

function fetchLogs()
{
	console.log("fetching logs");
	
	$('#logs_container').empty();
	
	//let start =  firebase.firestore.Timestamp.fromDate( new Date(parseInt(moment(new Date($('#start_date').val())).format("X"))) );
	//let end = firebase.firestore.Timestamp.fromDate(new Date(parseInt(moment(new Date($('#end_date').val())).format("X"))) );
	
	/* let start =  new Date($('#start_date').val());
	let end = new Date($('#end_date').val()); */
	
	let start =  new Date($('#start_date').val());
	let end = new Date($('#end_date').val());
	end.addHours(23);
	end.addMinutes(59);
	
	console.log(start);
	console.log(end);
	
	db.collection("logs")
	.where('date', '>=', start)
	.where('date', '<=', end)
	.orderBy('date', 'desc')
	
	.get()
    .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots
			var key = doc.id;
			var data = doc.data();					                    

			let msg = data['message'];
			let date = moment(data['date'].toDate()).format("MM-DD-YYYY HH:mm:ss");
			let toAppend = getTemplateDefault(msg, date);
			if(data['action'] == 'removal')
				toAppend = getTemplateRemove(msg, date);
			if(data['action'] == 'addition')
				toAppend = getTemplateAdd(msg, date);
			var newRow = $("#logs_container").append(toAppend);
			
			newRow.attr('data-key', key);
        });
    })
    .catch((error) => {
        console.log("Error getting documents: ", error);
    });
        /* .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                var key = change.doc.id;
                
                if (change.type === "removed") {
					$(".log['data-key="+key+"']").remove();
                }
                else
                {
                    var data = change.doc.data();
					                    

                    if (change.type === "added") {
						let msg = data['message'];
						let date = moment(data['date'].toDate()).format("MM-DD-YYYY HH:mm:ss");
                        let toAppend = getTemplateDefault(msg, date);
						if(data['action'] == 'removal')
							toAppend = getTemplateRemove(msg, date);
						if(data['action'] == 'addition')
							toAppend = getTemplateAdd(msg, date);
						var newRow = $("#logs_container").append(toAppend);
						
						newRow.attr('data-key', key);
                    }
                    
                    if (change.type === "modified") {
    
       
                    }                    
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {showLoading(false);});
        },

        function(error){

            bootbox.alert({
                centerVertical:true,
                size:'small',
                message:error.message
            }
                );
        }); */
}



function addGradeLog(msg, act)
{
	var now = new Date();
	var data = {
		date : firebase.firestore.Timestamp.fromDate(now),
		message : msg,
		type : "grade",
		action : act
	};	
	
	
	db.collection("logs").doc()
	.set(data, {merge:true});
}

function addAccountLog(msg, act)
{
	var now = new Date();
	var data = {
		date : firebase.firestore.Timestamp.fromDate(now),
		message : msg,
		type : "account",
		action : act
	};	
	
	
	db.collection("logs").doc()
	.set(data, {merge:true});
}

function addStudentLog(msg, act)
{
	var now = new Date();
	var data = {
		date : firebase.firestore.Timestamp.fromDate(now),
		message : msg,
		type : "student",
		action : act
	};	
	
	
	db.collection("logs").doc()
	.set(data, {merge:true});
}

function getTemplateDefault(txt, date)
{
	return '<li class="log list-group-item">'+txt+'<span class="badge badge-primary badge-pill float-right">'+date+'</span></li>';
}

function getTemplateRemove(txt, date)
{
	return '<li class="log list-group-item list-group-item-danger">'+txt+'<span class="badge badge-primary badge-pill float-right">'+date+'</span></li>';
}

function getTemplateAdd(txt, date)
{
	return '<li class="log list-group-item list-group-item-success">'+txt+'<span class="badge badge-primary badge-pill float-right">'+date+'</span></li>';
}


function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}