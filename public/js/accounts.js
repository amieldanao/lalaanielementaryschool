// (function() {
    
// var roles_string = ['Viewer', 'Editor', 'Admin'];


// addUserCallback();
// function addUserCallback()
// {
//     let email = $("#user_email").text();

//     let query = db.collection('users').where('email', '==', email);

//     query.onSnapshot(function(snapshot) {
//         snapshot.docChanges().forEach(function(change) {


//             if (change.type === "added") {
//                 let role = change.doc.data().role;
//                 initial_role = role;
//                 setRole(role);
//                 if(role == 2 || role == 1)
//                 {
//                     $("#table_tr th").last().attr('id', 'actions_th');
//                 }
//             }

//             if (change.type === "modified") {
//                 setRole(change.doc.data().role);
//                 $("body").empty();
//                 $.blockUI({fadeIn: 1, message: "Your user credential has changed. <br> You need to login again."});

//                  setTimeout(() => {
//                     location.replace("index.html");
//                 }, 3000);
//             }

//             if (change.type === "removed") {
//             }
//         });
//     },
//         function(error){
//         console.log('retry add user callback');
//         addUserCallback();
//     });
// }

// function setRole(newRole)
// {
//     switch(newRole)
//     {
//         case 0 : $(".role_editor").remove(); $(".role_admin").remove();break;
//         case 1 : $(".role_admin").remove();break;
//     }
    
//     $("#user_role").text("Role: " + roles_string[newRole]);
//     $("#user_role").attr("data-done", true);
// }

// })();

'user strict'


var targetNode = document.getElementById('user_role');

var config = { attributes: true, childList: true, subtree: true };

var callback = function(mutationsList, observer) {
    for(let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            
        }
        else if (mutation.type === 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');

            console.log('A child node has been added or removed.');
            Start();
        }
    }
};

var observer = new MutationObserver(callback);




$(document).ready( function () {
    // observer.observe(targetNode, config);
    Start();
});

function Start()
{
    // initVariables();
    // attachButtonCallbacks();
    // initializeStudentsTable();
    // reloadStudentsDB();
    sessionStorage.setItem("toEdit", '');
    addButtonsCallback();
    initTable();
}


var accounts_table;
var localAccountsData = [];

function initTable()
{
    let btns = [];
    if(document.getElementById("actions_th") != null)
    {
        console.log("has action column!");
        // myColumns.push({"sClass": "actions_buttons" });
        btns = [
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ]
    }



    accounts_table = $('#accounts_table').DataTable(
        {
        dom: 'Bflrtip',
        "scrollX": true,
        stateSave: true,
        columnDefs: [
        ],
        buttons: btns,        
        aoColumns: [
            { sClass: 'email center' },
            { sClass: 'l_name' },
            { sClass: 'f_name' },
            { sClass: 'status' },
            {
                bSortable: false,
                sClass: 'actions center'
            }
        ],
        initComplete: function(settings, json) {
            let buttons = $("#accounts_table_wrapper .dt-buttons button");
            buttons.addClass("btn");
            buttons.addClass("btn-outline-primary");
            buttons.removeClass("dt-button");

            addDBListener();
        }
    }
    );
}

function allowPassword(passw)
{
	
	return false;
}

function goSubmit(e)
{
    showLoading(true, "Saving...");
    var key = sessionStorage.getItem('toEdit');
    var isNew = (key == undefined || key.length == 0);
    // Check if ID or Email already exist.
    let email_query = db.collection('accounts').where('email', '==', $("#account_email").val());

    try
    {
        // We need to check if any admin is already using this email
        email_query.get().then(function(querySnapshot) {

            var re_edit = false;

            let requests = querySnapshot.docChanges().map((change) => {
                // doc.data() is never undefined for query doc snapshots
                // console.log(doc.id, " => ", doc.data());
                if(key == change.doc.id)
                    re_edit = true;

                return new Promise((resolve) => {
                    asyncFunction(change, resolve);
                });
            });
  
              Promise.all(requests).then(() => {
                  

                if(querySnapshot.size > 0 && re_edit == false && isNew == false)
                {
                    showLoading(false);
                    bootbox.alert({
                        message:"Email already in use!",
                        centerVertical: true,
                        className : "topmost-modal",
                        size : 'small'
                    });
                    // sessionStorage.setItem("toEdit", '');
    
                    return;
                }
                else
                {
                    var ref = db.collection("accounts").doc();
    
                    if(key == undefined || key.length == 0)
                    {
                        key = ref.id;
                    }
                    else
                    {
                        ref = db.collection("accounts").doc(key);
                    }
                    
                    sessionStorage.setItem("toEdit", "");
    
                    let data = 
                    {
                        email: $("#account_email").val(),
                        first_name : $("#first_name").val().toLowerCase(),
                        last_name : $("#last_name").val().toLowerCase(),
                        status : $("#status").val()
                    };
    
                    if(isNew)
                    {
                        secondaryApp.auth().createUserWithEmailAndPassword($("#account_email").val(), $("#account_email").val())
                        .then(function(result) {
                            
                            secondaryApp.auth().sendPasswordResetEmail(data.email).then(function() {
                                PNotify.alert({
                                    title:'New Account added successfully.',
                                    text : 'Password reset sent to ' + data.email
                                });
    
                                secondaryApp.auth().signOut();
    
                                data['password'] = $("#account_email").val();
                                setDataOnDBFromForm(data, ref, true);
                            })
                            .catch(function(error){
                                PNotify.error({
                                    title: 'Error',
                                    text: error
                                });
                                showLoading(false);
                            });
                            
                        })
                        .catch(error => {
                            // console.error("Error adding document: ", error);
                            PNotify.error({
                                title: 'Error',
                                text: error
                            });
                            showLoading(false);
    
                        });
                    }
                    else
                    {
                        setDataOnDBFromForm(data, ref, false);
                    }
    
                }


            });
    

            
        })
        .catch(error => {
            // console.error("Error adding document: ", error);
            PNotify.error({
                title: 'Error',
                text: error
            });
            showLoading(false);

        });
            
    }
    catch(error)
    {
        showLoading(false);
        bootbox.alert(error.message);        
    }
}


function addDBListener()
{
    showLoading(true, "Loading accounts...");
    try {

        db.collection("accounts")
        .onSnapshot(function(snapshot) { 
            let requests = snapshot.docChanges().map((change) => {
                var key = change.doc.id;
                
                if (change.type === "removed") {

                    delete localAccountsData[change.doc.data().email];

                    accounts_table.row('#'+key).remove().draw();                   
                }
                else
                {
                    let data = change.doc.data();
                    localAccountsData[data.email] = data;
                    localAccountsData[data.email]["key"] = change.doc.id;
                    
                    var d = {grade:"", section:"", last_name:"", name_ext_name:"", status:""};
                    if('email' in change.doc.data())
                        d['email'] = change.doc.data().email;
                    
                    if('first_name' in change.doc.data())
                        d['first_name'] = toTitleCase(change.doc.data().first_name);

                    if('last_name' in change.doc.data())
                        d['last_name'] = toTitleCase(change.doc.data().last_name);

                    if('status' in change.doc.data())
                        d['status'] = toTitleCase(change.doc.data().status);

                    let newData =
                    [
                        d.email,
                        d.last_name,
                        d.first_name,
                        d.status     
                    ];

                    if (change.type === "added") {
                        // if(document.getElementById("actions_th") != null)
                        newData.push(getNewActionRow(change.doc.id));
                        // else
                        //     newData.push('<a data-key="'+change.doc.id+'" href="#" onclick="clickView(this);" class="on-default edit-row mx-2"><i class="fas fa-eye"></i></a>');              
                        let newRow = accounts_table.row.add(newData).node().id = change.doc.id;
    
                        accounts_table.draw( false );
                    }
                    
                    if (change.type === "modified") {
    
                        // let data = change.doc.data();
                        // localStudentsData[data.LRN] = data;
                        // localStudentsData[data.LRN]["key"] = change.doc.id; 
                        let tr = $('.remove-row[data-key="'+key+'"]').parent().parent();                    
    
                        let updatedData = [
                            d.email,
                            toTitleCase(d.last_name),
                            toTitleCase(d.first_name), 
                            d.status,            
                            getNewActionRow(key)
                        ];
    
                        accounts_table.row(tr).data(updatedData).draw();                 
                    }                    
                }
                
                return new Promise((resolve) => {
                  asyncFunction(change, resolve);
                });
            });

            Promise.all(requests).then(() => {showLoading(false);});
        },

        function(error){

            bootbox.alert({
                centerVertical:true,
                size:'small',
                message:error.message
            }
                );
        });
    }
    catch(err) {
        bootbox.alert(
        {
            message:err.message,
            onHidden: function () {
                UpdateModalScrolling();
            }
        });
    }
}

function getNewActionRow(key)
{
    return  '<a data-key="'+key+'" href="#" onclick="showPromptEdit(this);" class="on-default edit-row mx-2" data-toggle="tooltip" data-placement="top" title="edit student information"><i class="fa fa-pencil"></i></a>'+
            '<a data-key="'+key+'" href="#" onclick="showPromptDelete(this);" class="on-default remove-row mx-2" data-toggle="tooltip" data-placement="top" title="delete student" style="color:red;"><i class="fa fa-trash-o"></i></a>';
}


function setDataOnDBFromForm(data, ref, isNew)
{
    ref.set(data, {merge:true})
    .then(function(docRef) {

        showNotifSuccess('Saved', data.email + ' saved successfully');
        showLoading(false);

        // sessionStorage.setItem("toEdit", '');
		if(isNew)
			addAccountLog('New account added ' + data['email'] + " added by " + $("#user_email").text(), "addition");
		else
			addAccountLog('Account updated' + data['email'] + " updated by " + $("#user_email").text(), "default");

        $('#editAccountModal').modal('hide');
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
        PNotify.error({
          title: 'Error',
          text: error
      });
        showLoading(false);
    });
}


function showPromptDelete(elem)
{
    var key = $(elem).attr('data-key');
    var tr = $(elem).parent().parent();

    var myRow = accounts_table.row(tr);

    let EMAIL = myRow.data()[0];

    bootbox.confirm({ 
        size: "large",
        centerVertical : true,
        title: "Are you sure?",
        message : "Continue deleting this account?",
        callback: function(result){ 
            if(result)
            {
                showLoading(true, "Deleting...");
                try
                {
                    db.collection("accounts").doc(key).delete().then(function() {
                        showNotifSuccess(EMAIL + " account successfully deleted!");
                    }).catch(function(error) {
                        showNotifError("Error removing document: " + error.message);
                    });                    
                }
                catch(error)
                {
                    showLoading(false);
                    bootbox.alert(error.message);
                }
            }

            sessionStorage.setItem("toEdit", '');
        }
    })
}

function showPromptEdit(elem)
{
	$("#editAccountModalLabel").text("Manage Account");
    var key = $(elem).attr('data-key');
    sessionStorage.setItem("toEdit", key);
    
    var isNew = (key == undefined || key.length == 0);

    var tr = $(elem).parent().parent();

    var myRow = accounts_table.row(tr);

    let EMAIL = myRow.data()[0];
    $("#account_email").val(EMAIL);

    

    $("#last_name").val(myRow.data()[1]);
    $("#first_name").val(myRow.data()[2]);  
    $("#status").val(myRow.data()[3]);

    $("#editAccountModal").modal('show');
}


function addButtonsCallback()
{
    $('#editAccountModal').modal(
    {
        keyboard: false,
        backdrop: 'static',
        show:false
    });

    $("#addToTable").on('click', function(){
        sessionStorage.setItem('toEdit', '');
        $("#editAccountForm").get(0).reset();
		$("#editAccountModalLabel").text("Add Account");
    });

    $("#importToTable").on('click', function(){
        showImportModal();
    });

    $("#editAccountForm").keypress(function(event){

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          if($("#editAccountForm").get(0).reportValidity())
            goSubmit(event);
          //alert('You pressed a "enter" key in somewhere');
        }
      });

      $("#submitButton").on('click', function(e){
        if($("#editAccountForm").get(0).reportValidity())
          goSubmit(e);
      });

    $("#editAccountModal").on('hidden.bs.modal', function(m){
        sessionStorage.setItem('toEdit', '');
    });

    $("#editAccountModal").on('show.bs.modal', function () {
        var key = sessionStorage.getItem("toEdit");
        
        var isNew = (key == undefined || key.length == 0);
        $("#account_email").prop('readonly', !isNew);
    });

    addPastePrevent($("form input"));

}


function asyncFunction (item, cb) {
  setTimeout(() => {
      
    cb();
  }, 100);
}

var myConfig = {
    apiKey: "AIzaSyDGufhY3bFgaNqJptXhDDDqb-VNwONNJUI",
    authDomain: "lalaan-i-elementary-school.firebaseapp.com",
    databaseURL: "https://lalaan-i-elementary-school.firebaseio.com",
    projectId: "lalaan-i-elementary-school",
    storageBucket: "lalaan-i-elementary-school.appspot.com",
    messagingSenderId: "666792407363",
    appId: "1:666792407363:web:875dccf6cd068f71d0bff6"
};

var secondaryApp = firebase.initializeApp(myConfig, "Secondary");